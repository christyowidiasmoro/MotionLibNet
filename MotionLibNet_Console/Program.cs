﻿using System;
using System.Collections.Generic;
using System.Drawing;
using QuickGraph;
using QuickGraph.Algorithms;
using QuickGraph.Algorithms.RandomWalks;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
//using MotionLibNet.Graph;

using MotionLibNet;
using MotionLibNet.IO;
using MotionLibNet.Algorithm.MotionGraphs;

namespace MotionLibNet_Console
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Console.Out.WriteLine("MotionLibNet Console");

            TestMotionFields();

            //float threshold = 100;
            //if (args.Length > 1)
            //    threshold = float.Parse(args[0]);
            //TestMotionGraph(threshold, false);

            //TestClone();

            //TestBvh();

            //TestBlending();
            //MotionGraphs.Pair pair;
            //FileManager.DeserializeFromFile<MotionGraphs.Pair>("pair_16_11_walk, veer left.bvh 16_13_walk, veer right.bvh", out pair);

            //float[,] values = pair.Distances;
            //float[,] values = new float[,] { { 10, 1, 2, 3, 4, 5, 6, 7, 8, 9, 9, 4, 5, 2, 1, 4, 5, 63, 1, 9 } };

            //List<Vector2> minimums = new List<Vector2>();
            //TestLocalMinima(values, 0, 0, values.GetLength(0), values.GetLength(1), ref minimums);

            //SaveImageData(values, minimums, "local_minimum.png");

            System.Console.In.Read();
        }

        static void SavePairImage(MotionGraphs.Pair pair, string filename)
        {
            byte[] byteArray = new byte[pair.FrameNumbers[0] * pair.FrameNumbers[1] * 3];

            float max = float.MinValue;
            float min = float.MaxValue;
            for (int i = 0; i < pair.FrameNumbers[0]; i++) {
                for (int j = 0; j < pair.FrameNumbers[1]; j++) {
                    if (pair.Distances[i, j] > max)
                        max = pair.Distances[i, j];
                    if (pair.Distances[i, j] < min)
                        min = pair.Distances[i, j];
                }
            }

            Bitmap bitmap = new Bitmap(pair.FrameNumbers[0], pair.FrameNumbers[1], PixelFormat.Format24bppRgb);

            for (int i = 0; i < pair.FrameNumbers[0]; i++)
            {
                for (int j = 0; j < pair.FrameNumbers[1]; j++)
                {
                    int r = (int) ((max - pair.Distances[i, j]) / (max - min) * 255);
                    int g = r;
                    int b = r;

                    bitmap.SetPixel(i, j, System.Drawing.Color.FromArgb(r, g, b));
                }
            }

            max = float.MinValue;
            min = float.MaxValue;
            for (int i = 0; i < pair.LocalMinima.Count; i++)
            {
                int x = (int)(pair.LocalMinima[i].x);
                int y = (int)(pair.LocalMinima[i].y);

                if (pair.Distances[x, y] > max)
                    max = pair.Distances[x, y];
                if (pair.Distances[x, y] < min)
                    min = pair.Distances[x, y];
            }

            for (int i = 0; i < pair.LocalMinima.Count; i++)
            {
                int x = (int)(pair.LocalMinima[i].x);
                int y = (int)(pair.LocalMinima[i].y);

                int g = 50 + (int)((max - pair.Distances[x, y]) / (max - min) * 200);

                bitmap.SetPixel(x, y, System.Drawing.Color.FromArgb(0, g, 0));

//                if (pair.Distances[x, y] < 100)
//                    bitmap.SetPixel(x, y, System.Drawing.Color.Red);
//                else
//                    bitmap.SetPixel(x, y, System.Drawing.Color.Green);
            }

            bitmap.Save(filename);
        }

        static void TestMotionGraph(float threshold, bool load)
        {
            string directory = "../../../../bvh/cmu/";
            //string[] filenames = FileManager.GetAllFiles(directory, "*.bvh");
            string[] filenames = {
                "16_01_jump.bvh",
                "16_05_forward jump.bvh",
                "16_08_run&jog, sudden stop.bvh",
                "16_11_walk, veer left.bvh",
                "16_13_walk, veer right.bvh",
                "16_15_walk.bvh",
                "16_35_run&jog.bvh",
                "16_27_walk, 90-degree left turn.bvh",
                "16_29_walk, 90-degree right turn.bvh"
/*
                "16_11_walk, veer left.bvh",            //133
                "16_13_walk, veer right.bvh",           //111
                "16_15_walk.bvh",                       //117
                "16_17_walk, 90-degree left turn.bvh",  //129
                "16_19_walk, 90-degree right turn.bvh"  //102 
                */  
            };                                          //=592

            Dictionary<string, Motion> dict = new Dictionary<string, Motion>();
            BVHLoader loader = new BVHLoader();

            Console.Out.WriteLine("Reading BVH file...");
            int index = 0;
            foreach(string filename in filenames)
            {
                Console.Out.WriteLine(directory + filename);

                Motion motion = loader.Load(directory + filename);
                motion.Name = filename;
                                
                dict.Add(filename, motion);
                index++;
            }

            
            MotionGraphs mg = new MotionGraphs(new MotionGraphsDistance());

            Console.Out.WriteLine("Motion Graphs - Construct Map ...");

            index = 0;
            foreach (string key1 in filenames)
            {
                foreach (string key2 in filenames)
                {
                    string fname = "pair_" + key1 + "_" + key2;

                    Console.Out.Write(fname + " ...");

                    if (System.IO.File.Exists(fname) && load)
                    {
                        Console.Out.Write("\b\b\bload");

                        MotionGraphs.Pair pair;

                        FileManager.DeserializeFromFile<MotionGraphs.Pair>(fname, out pair);

                        mg.UpdateDatabase(dict[key1]);
                        mg.UpdateDatabase(dict[key2]);

                        mg.SetDistancePair(index, pair);

                        Console.Out.WriteLine("ed");
                    }
                    else
                    {
                        Console.Out.Write("\b\b\bcreate");

                        mg.DetectCandidateTransition(dict[key1], dict[key2]);

                        MotionGraphs.Pair pair = mg.GetDistancePair(index);

                        SavePairImage(pair, fname + ".jpg");

                        FileManager.SerializeToFile(fname, pair);

                        Console.Out.WriteLine("\b\b\b\b\b\bdone");
                    }
                    index++;
                }
            }

            if (System.IO.File.Exists("all.graph") && load)
            {
                Console.Out.Write("Loading Graph ...");

                mg.LoadGraph("all.graph");

                mg.PruneGraph();

                mg.GraphToDot("prune.graph");

                Console.Out.WriteLine("\b\b\bDone");
            }
            else
            {
                Console.Out.Write("Creating Graph ...");

                mg.CreateGraph(threshold);

                mg.SaveGraph("all.graph");

                mg.GraphToDot("all.graph");

                mg.PruneGraph();

                mg.GraphToDot("prune.graph");


                Console.Out.WriteLine("\b\b\bDone");
            }

            /*
            if (System.IO.File.Exists("random.walk.graph"))
            {
                Console.Out.Write("Loading Random Walk ...");

                mg.LoadRandomWalkGraph("random.walk.graph");

                Console.Out.WriteLine("\b\b\bDone");
            }
            else */
            {
                Console.Out.WriteLine("Generating Random Walk ...");

                Motion motion = null;
                mg.RandomWalk(ref motion, 10, 100, true);

                mg.RandomWalk(ref motion, 10, 100, false);

                mg.RandomWalk(ref motion, 10, 100, false);

                mg.RandomWalk(ref motion, 10, 100, false);

                mg.RandomWalk(ref motion, 10, 100, false);

                mg.SaveRandomWalkGraph("random.walk.graph");

                mg.RandomWalkGraphToDot("random.walk.graph");

                loader.Save("randomwalk_blending.bvh", motion);

            }
        }

        static void TestMotionFields()
        {
            string directory = "../../../../bvh/cmu/";
            string[] filenames = {
//                "16_11_walk, veer left.bvh",
//                "16_13_walk, veer right.bvh",
                "16_15_walk.bvh",
            };

            Dictionary<string, Motion> dict = new Dictionary<string, Motion>();
            BVHLoader loader = new BVHLoader();

            Console.Out.WriteLine("Reading BVH file...");
            int index = 0;
            foreach (string filename in filenames)
            {
                Console.Out.WriteLine(directory + filename);

                Motion motion = loader.Load(directory + filename);
                motion.Name = filename;

                dict.Add(filename, motion);
                index++;
            }

            MotionFields mf = new MotionFields();

            foreach (string key in filenames)
            {
                mf.AddMotionData(dict[key]);
            }
        }

        static void TestBvh()
        {
            Dictionary<string, Vector3> positions = new Dictionary<string, Vector3>();
            Dictionary<string, Quaternion> rotations = new Dictionary<string, Quaternion>();

            // Load
            BVHLoader loader = new BVHLoader();
            loader.Load("../../../../bvh/cmu/16_01_jump.bvh");
            //loader.Load("example3.bvh");

            Motion motion = loader.GetMotion(0);

            Console.Out.WriteLine(motion.RootJoint.ToString());
            motion.onJointUpdated = new Motion.OnJointUpdated((m, joint) => {
                Console.WriteLine("1:: {0} {1} {2}", joint.Name, joint.Position.ToString(), joint.Rotation.ToString());
                string name = joint.Name;
                if (joint.Parent != null)
                    name = joint.Parent.Name + "-" + name;
                positions.Add(name, joint.Position);
                rotations.Add(name, joint.Rotation);
            });

            Joint J;

            motion.MoveTo(1, out J);
            motion.UpdateJointObject();

            Console.WriteLine("================");

            // Save
            Motion motion2 = motion.CopyStructure();
            motion2.NumFrames = 1;
            motion2.RecordTo(0, J);
            motion2.onJointUpdated = new Motion.OnJointUpdated((m, joint) => {
                Console.WriteLine("2:: {0} {1} {2}", joint.Name, joint.Position.ToString(), joint.Rotation.ToString());
                string name = joint.Name;
                if (joint.Parent != null)
                    name = joint.Parent.Name + "-" + name;
                if (positions.ContainsKey(name))
                {
                    if (positions[name] == joint.Position)
                        Console.WriteLine(" Position OK " + positions[name].ToString() + " " + joint.Position.ToString());
                    else
                        Console.WriteLine(" Position NO <<< " + positions[name].ToString() + " " + joint.Position.ToString());

                    if (rotations[name].Equals(joint.Rotation))
                        Console.WriteLine(" Rotation OK " + rotations[name].ToString() + " " + joint.Rotation.ToString());
                    else
                        Console.WriteLine(" Rotation NO <<< " + rotations[name].ToString() + " " + joint.Rotation.ToString());
                }
            });

            Console.WriteLine("================");

            motion2.MoveTo(0);
            motion2.UpdateJointObject();

            loader.Save("test-save.bvh", motion2);
        }

        static void TestScc()
        {
            AdjacencyGraph<string, TaggedEdge<string, float>> graph = new AdjacencyGraph<string, TaggedEdge<string, float>>();

            graph.AddVertex("1");
            graph.AddVertex("2");
            graph.AddVertex("3");
            graph.AddVertex("4");
            graph.AddVertex("5");
            graph.AddVertex("6");
            graph.AddVertex("7");
            graph.AddVertex("8");

            graph.AddEdge(new TaggedEdge<string, float>("1", "2", 1));
            graph.AddEdge(new TaggedEdge<string, float>("2", "3", 1));
            graph.AddEdge(new TaggedEdge<string, float>("3", "1", 1));
            graph.AddEdge(new TaggedEdge<string, float>("4", "2", 1));
            graph.AddEdge(new TaggedEdge<string, float>("4", "3", 1));
            graph.AddEdge(new TaggedEdge<string, float>("4", "5", 1));
            graph.AddEdge(new TaggedEdge<string, float>("5", "4", 1));
            graph.AddEdge(new TaggedEdge<string, float>("5", "6", 1));
            graph.AddEdge(new TaggedEdge<string, float>("6", "3", 1));
            graph.AddEdge(new TaggedEdge<string, float>("6", "7", 1));
            graph.AddEdge(new TaggedEdge<string, float>("7", "6", 1));
            graph.AddEdge(new TaggedEdge<string, float>("8", "5", 1));
            graph.AddEdge(new TaggedEdge<string, float>("8", "7", 1));
            graph.AddEdge(new TaggedEdge<string, float>("8", "8", 1));

            RandomWalkAlgorithm<string, TaggedEdge<string, float>> walk = new RandomWalkAlgorithm<string, TaggedEdge<string, float>>(graph);
            walk.TreeEdge += Walk_TreeEdge;
            walk.Generate("1");


            StronglyConnectedComponentsAlgorithm<string, TaggedEdge<string, float>> scc =
                new StronglyConnectedComponentsAlgorithm<string, TaggedEdge<string, float>>(graph);

            scc.Compute();

            if (scc.ComponentCount != 1) // this graph has multiple components
            {
                Console.WriteLine("Graph is not strongly connected");
                Console.WriteLine("Graph contains {0} strongly connected components", scc.ComponentCount);
                foreach (var kv in scc.Components)
                {
                    Console.WriteLine("Vertex {0} is connected to subgraph {1}", kv.Key, kv.Value);
                }
            }
        }

        private static void Walk_TreeEdge(object sender, EdgeEventArgs<string, TaggedEdge<string, float>> e)
        {
            Console.WriteLine(string.Format("{0} -> {1}", e.Edge.Source, e.Edge.Target));
        }

        static void TestMatrix()
        {
            Quaternion q = Quaternion.AngleAxis(45, Vector3.right);
            Console.Out.WriteLine("Quaternion = " + q.ToString());

            Vector3 axis;
            float angle;
            q.ToAxisAngle(out axis, out angle);
            Console.Out.WriteLine("Axis = " + axis.ToString() + " Angle = " + angle);

            Matrix4x4 m = Matrix4x4.TRS(new Vector3(5, 10, 20), q, new Vector3(1, 2, 3));
            Console.Out.WriteLine("Matrix = " + m.ToString());
        }
        
        static void TestBlending()
        {
            string directory = "../../../../bvh/cmu/";
            string[] filenames = {
                "16_19_walk, 90-degree right turn.bvh",  //102   > frame 12
                "16_15_walk.bvh"                       //117     > frame 43
            };                                          //=592

            Dictionary<string, Motion> dict = new Dictionary<string, Motion>();
            BVHLoader loader = new BVHLoader();

            Console.Out.WriteLine("Test Blending file...");

            Motion motion1 = loader.Load(directory + filenames[0]);
            motion1.Name = filenames[0];

            Motion motion2 = loader.Load(directory + filenames[1]);
            motion2.Name = filenames[0];

            Motion result = motion1.CopyStructure();
            result.NumFrames = 0;

            // first motion frame
            for (int f = 0; f < 12; f++)
            {
                motion1.MoveTo(f);
                result.RecordTo(f, motion1.RootJoint);
            }

            //Vector3 delta = motion2.RootJoint.Position - motion1.RootJoint.Position;
            //Console.Out.WriteLine("Delta = {0}", delta);

            // next motion frame
            for (int f = 43; f < motion2.NumFrames; f++)
            {
                motion2.MoveTo(f);

                //motion2.RootJoint.Position -= delta;
                result.RecordTo(f - 31, motion2.RootJoint);
            }

            // blending
            int i = 12, j = 43, k = 10;

            MotionGraphs mg = new MotionGraphs(new MotionGraphsDistance());

            motion1.MoveTo(i);

            List<Vector3> pA = new List<Vector3>();
            List<Quaternion> qA = new List<Quaternion>();
            motion1.RootJoint.ExtractJointPosition(ref pA);
            motion1.RootJoint.ExtractJointRotation(ref qA);

            motion2.MoveTo(j);

            List<Vector3> pB = new List<Vector3>();
            List<Quaternion> qB = new List<Quaternion>();
            motion2.RootJoint.ExtractJointPosition(ref pB);
            motion2.RootJoint.ExtractJointRotation(ref qB);
            
            mg.getDistanceAlgorithm().CalculateParamOnly(pA, pB);
            Console.Out.WriteLine(mg.getDistanceAlgorithm().PrintParam());

            /*
            List<Vector3> pC = new List<Vector3>();
            mg.Calculate2DTransform(pA, pB, mg.GenerateJointWeight(), out pC);
            FileManager.SaveList("pc.txt", new System.Collections.ArrayList(pA));
            FileManager.SaveList("pc.txt", new System.Collections.ArrayList(pB));
            FileManager.SaveList("pc.txt", new System.Collections.ArrayList(pC));

            float distance = mg.CalculateDistance(pA, pB);
            Console.Out.WriteLine("Distance = {0}", distance);
            */

            for (int p = 0; p < k; p++)
            {
                //float a1 = (p + 1) / k;
                //float alpha = 2 * a1 * a1 * a1 - 3 * a1 * a1 + 1;

                motion1.MoveTo(i + p);
                motion2.MoveTo(j - k + 1 + p);

                Joint rootJoint = motion2.RootJoint;

                rootJoint.Position = mg.getDistanceAlgorithm().CalculatePositionFromParam(motion2.RootJoint.Position);

                //rootJoint.Position = alpha * motion1.RootJoint.Position + (1 - alpha) * motion2.RootJoint.Position;

                result.InsertTo(i + p + 1, rootJoint);
            }

            loader.Save("blending_test.bvh", result);
        }

        static void TestClone()
        {
            Vector2 vec21 = new Vector2 { x = 10, y = 20 };
            Vector2 vec22 = vec21;
            vec22.x = 30;
            Console.Out.WriteLine("Vector2 {0} === {1} = {2}", vec21, vec22, vec21.Equals(vec22));

            Vector3 vec31 = new Vector3 { x = 10, y = 20, z = 30 };
            Vector3 vec32 = vec31;
            vec32.x = 40;
            Console.Out.WriteLine("Vector3 {0} === {1} = {2}", vec31, vec32, vec31.Equals(vec32));

            Vector4 vec41 = new Vector4 { x = 10, y = 20, z = 30, w = 40 };
            Vector4 vec42 = vec41;
            vec42.x = 50;
            Console.Out.WriteLine("Vector4 {0} === {1} = {2}", vec41, vec42, vec41.Equals(vec42));

            Quaternion q1 = new Quaternion { x = 10, y = 20, z = 30, w = 40 };
            Quaternion q2 = q1;
            q2.x = 50;
            Console.Out.WriteLine("Quaternion {0} === {1} = {2}", q1, q2, q1.Equals(q2));

            Matrix4x4 m1 = Matrix4x4.identity;
            Matrix4x4 m2 = m1;
            m2.m00 = 999;
            Console.Out.WriteLine("Matrix4x4 {0} === {1} = {2}", m1, m2, m1.Equals(m2));

            string directory = "../../../../bvh/cmu/";
            string filename = "16_15_walk.bvh";

            BVHLoader loader = new BVHLoader();
            Motion motion1 = loader.Load(directory + filename);
            motion1.Name = filename;
            Motion motion2 = motion1.Clone();
            motion2.RootJoint.Name = filename;
            motion2.FrameTime = 1000;
            motion2.Data[0] = 1000;
            Console.Out.WriteLine("Motion {0} === {1} = {2}", motion1.RootJoint.Name, motion2.RootJoint.Name, motion1.Equals(motion2));
            Console.Out.WriteLine("Motion Fields {0} === {1} = {2}", motion1.FrameTime, motion2.FrameTime, motion1.Equals(motion2));
            Console.Out.WriteLine("Motion Data {0} === {1} = {2}", motion1.Data[0], motion2.Data[0], motion1.Data.Equals(motion2.Data));

            Joint j1 = motion1.RootJoint;   // by references
            Joint j2 = j1.Clone();          // deep copy
            j1.Name = "changed name";
            Console.Out.WriteLine("Joint {0} === {1} = {2}", j1.Name, j2.Name, j1.Equals(j2));
            ((Joint)j2.Children[0]).Name = "changed children";
            j2.ChannelsOrder[0] = Joint.Channel.Yrotation;
            Console.Out.WriteLine("Joint Fields {0} === {1} = {2}", j1.ChannelsOrder[0], j2.ChannelsOrder[0], j1.Equals(j2));
            Console.Out.WriteLine("Joint Child  {0} === {1} = {2}", ((Joint)j1.Children[0]).Name, ((Joint)j2.Children[0]).Name, j1.Equals(j2));
        }

        static void SaveImageData(float[,] values, List<Vector2> mins, string filename)
        {
            byte[] byteArray = new byte[values.GetLength(0) * values.GetLength(1) * 3];

            float max = float.MinValue;
            float min = float.MaxValue;
            for (int i = 0; i < values.GetLength(0); i++)
            {
                for (int j = 0; j < values.GetLength(1); j++)
                {
                    if (values[i, j] > max)
                        max = values[i, j];
                    if (values[i, j] < min)
                        min = values[i, j];
                }
            }

            Bitmap bitmap = new Bitmap(values.GetLength(0), values.GetLength(1), PixelFormat.Format24bppRgb);

            for (int i = 0; i < values.GetLength(0); i++)
            {
                for (int j = 0; j < values.GetLength(1); j++)
                {
                    int r = (int)((max - values[i, j]) / (max - min) * 255);
                    int g = r;
                    int b = r;

                    bitmap.SetPixel(i, j, System.Drawing.Color.FromArgb(r, g, b));
                }
            }

            for (int i = 0; i < mins.Count; i++)
            {
               bitmap.SetPixel((int)mins[i].x, (int)mins[i].y, System.Drawing.Color.Green);
            }

            bitmap.Save(filename);
        }

    }
}
