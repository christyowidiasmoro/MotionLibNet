﻿using System;
using System.Collections.Generic;

namespace QuickGraph.Algorithms.RandomWalks
{
    [Serializable]
    public sealed class NoLoopMarkovEdgeChain<TVertex, TEdge> :
        MarkovEdgeChainBase<TVertex, TEdge>
        where TEdge : IEdge<TVertex>
    {
        private List<TVertex> visitedVertex = new List<TVertex>();

        public override TEdge Successor(IImplicitGraph<TVertex, TEdge> g, TVertex u)
        {
            int outDegree = g.OutDegree(u);
            if (outDegree == 0)
                return default(TEdge);

            List<int> index = new List<int>();
            for (int i = 0; i < outDegree; i++)
                index.Add(i);

            TEdge e = default(TEdge);
            while (index.Count > 0)
            {
                int i = this.Rand.Next(0, index.Count - 1);
                e = g.OutEdge(u, index[i]);
                if (!visitedVertex.Contains(e.Target))
                {
                    visitedVertex.Add(e.Target);
                    break;
                }
                index.Remove(index[i]);
            }

            return e;
        }
    }
}
