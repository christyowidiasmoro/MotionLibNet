﻿/*
 * License here
 * 
 * christyowidiasmoro (c.christyowidiasmoro@uu.nl, christyowidiasmoro@gmail.com)
 */
 
using System.IO;
using System.Collections;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace MotionLibNet.IO
{
    /// <summary>
    /// generic file management to save and load
    /// </summary>
    public class FileManager
    {
        /// <summary>
        /// save object to string
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="filename"></param>
        /// <param name="value"></param>
        public static void SaveString<T>(string filename, T value)
        {
            using (StreamWriter sr = new StreamWriter(filename))
            {
                sr.Write(value.ToString());
            }
        }

        /// <summary>
        /// read string from file
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public static string LoadString(string filename)
        {
            using (StreamReader sr = new StreamReader(filename))
            {
                string s = sr.ReadToEnd();
                return s;
            }
        }

        /// <summary>
        /// serialize object to file
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="obj"></param>
        public static void SerializeToFile(string filename, object obj)
        {
            IFormatter formatter = new BinaryFormatter();
            Stream stream = new FileStream(filename,
                                     FileMode.Create,
                                     FileAccess.Write, FileShare.None);
            formatter.Serialize(stream, obj);
            stream.Close();
        } 

        /// <summary>
        /// deserialize object from file
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="filename"></param>
        /// <param name="obj"></param>
        public static void DeserializeFromFile<T>(string filename, out T obj)
        {
            IFormatter formatter = new BinaryFormatter();
            Stream stream = new FileStream(filename,
                                      FileMode.Open,
                                      FileAccess.Read,
                                      FileShare.Read);
            obj = (T)formatter.Deserialize(stream);
            stream.Close();
        }

        /// <summary>
        /// save byte array to file image
        /// </summary>
        public static void SaveImage()
        {
//            Image x = (Bitmap)((new ImageConverter()).ConvertFrom(jpegByteArray));
        }

        /// <summary>
        /// save list to file
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="list"></param>
        public static void SaveList(string filename, ArrayList list)
        {
            using (StreamWriter sr = new StreamWriter(filename, true))
            {
                string s = "";
                for (int i = 0; i < list.Count; i++)
                    s += list[i] + " ";

                sr.WriteLine(s);
            }
        }

        /*
        /// <summary>
        /// save motion graph pair to file
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="pair"></param>
        public static void SavePair(string filename, MotionGraphs.Pair pair)
        {
            using (StreamWriter sr = new StreamWriter(filename))
            {
                string s = "";

                sr.WriteLine(pair.MotionName[0]);
                sr.WriteLine(pair.MotionName[1]);
                

                for (int i = 0; i < pair.FrameNumbers[0]; i++)
                {
                    s = "";
                    for (int j = 0; j < pair.FrameNumbers[1]; j++)
                    {
                        s += pair.Distances[i, j] + " ";
                    }
                    sr.WriteLine(s);
                }
            }
        }
        */
        
        /// <summary>
        /// get all files from specific directory
        /// </summary>
        /// <param name="directory"></param>
        /// <param name="filter"></param>
        /// <returns></returns>
        public static string[] GetAllFiles(string directory, string filter)
        {
            string[] result = System.IO.Directory.GetFiles(directory, filter, System.IO.SearchOption.AllDirectories);

            for (int i = 0; i < result.Length; i++)
                result[i] = System.IO.Path.GetFileName(result[i]);

            return result;
        }
    }
}
