﻿/*
 * License here
 * 
 * christyowidiasmoro (c.christyowidiasmoro@uu.nl, christyowidiasmoro@gmail.com)
 */

using QuickGraph;
using QuickGraph.Graphviz;

namespace MotionLibNet.IO
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class GraphDot<T>
    {
        public delegate int FormatVertexColoring(T node, T oldNode, ref int lastIndex);

        public event FormatVertexColoring VertexColoring;

        private T oldVertex;

        private int colorIndex;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="adjGraph"></param>
        /// <returns></returns>
        public string GraphToDot(string filename, BidirectionalGraph<T, TaggedEdge<T, float>> adjGraph = null)
        {
            var graphviz = new GraphvizAlgorithm<T, TaggedEdge<T, float>>(adjGraph);
            graphviz.FormatEdge += Graphviz_FormatEdge;
            graphviz.FormatVertex += Graphviz_FormatVertex;

            string output = graphviz.Generate(new FileDotEngine(), filename);

            return output;
        }

        /// <summary>
        /// function to format vertex
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Graphviz_FormatVertex(object sender, FormatVertexEventArgs<T> e)
        {
            e.VertexFormatter.Label = e.Vertex.ToString();
            e.VertexFormatter.Style = QuickGraph.Graphviz.Dot.GraphvizVertexStyle.Filled;

            if (VertexColoring != null)
                e.VertexFormatter.SetCustomParam("fillcolor", Color.Set312[VertexColoring(e.Vertex, oldVertex, ref colorIndex) % Color.Set312.Length]);

            oldVertex = e.Vertex;
        }

        /// <summary>
        /// function to format edge
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Graphviz_FormatEdge(object sender, FormatEdgeEventArgs<T, TaggedEdge<T, float>> e)
        {
            e.EdgeFormatter.Label.Value = e.Edge.Tag.ToString();
        }

    }
}
