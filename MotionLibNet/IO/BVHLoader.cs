﻿/*
 * License here
 * 
 * christyowidiasmoro (c.christyowidiasmoro@uu.nl, christyowidiasmoro@gmail.com)
 */
 
using System.IO;
using System;
using System.Collections;
using System.Globalization;
using System.Collections.Generic;

namespace MotionLibNet.IO
{
    /// <summary>
    /// 
    /// </summary>
    public class BVHLoader
    {
        /// <summary>
        /// 
        /// </summary>
        private List<Motion> motions = new List<Motion>();

        /// <summary>
        /// 
        /// </summary>
        private List<IEnumerator> tokens = new List<IEnumerator>();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="index"></param>
        /// <param name="joint"></param>
        public delegate void OnJointUpdated(int index, Joint joint);

        /// <summary>
        /// 
        /// </summary>
        public OnJointUpdated onJointUpdated = null;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public Motion GetMotion(int index)
        {
            return motions[index];
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public Joint GetRootJoint(int index)
        {
            return motions[index].RootJoint;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public string GetRootJointName(int index)
        {
            return motions[index].RootJoint.Name;
        }

        /// <summary>
        /// save motion to bvh file
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="motion"></param>
        public void Save(string filename, Motion motion)
        {
            StreamWriter writer = new StreamWriter(filename);

            writer.WriteLine("HIERARCHY");

            SaveJoint(writer, motion.RootJoint);

            SaveMotion(writer, motion);

            writer.Close();
        }

        /// <summary>
        /// load bvh file to motion data structure
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public Motion Load(string filename)
        {
            char[] delimiterChars = { ' ', '\t', '\n' };

            StreamReader reader = new StreamReader(filename);
            string ss = reader.ReadToEnd();

            ArrayList s = new ArrayList();
            s.AddRange(ss.Split(delimiterChars, StringSplitOptions.RemoveEmptyEntries));

            motions.Add(new Motion());
            int index = motions.Count - 1;

            tokens.Add(s.GetEnumerator());

            while (tokens[index].MoveNext())
            {
                if (tokens[index].Current.ToString().Trim().StartsWith("HIERARCHY"))
                    LoadHierarchy(index);
            }
            reader.Close();

            return motions[index];
        }

        /// <summary>
        /// save joint to bvh format
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="joint"></param>
        /// <param name="tab"></param>
        private void SaveJoint(StreamWriter writer, Joint joint, int tab = 0)
        {
            NumberFormatInfo nfi = new NumberFormatInfo();
            nfi.NumberDecimalSeparator = ".";
            nfi.NumberDecimalDigits = 6;

            string stab = "";
            for (int i = 0; i < tab; i++)
                stab += "\t";

            if (joint.Parent == null)
                writer.WriteLine("ROOT {0}", joint.Name);
            else if (joint.Children.Count == 0)
                writer.WriteLine("{0}End Site", stab);
            else
                writer.WriteLine("{0}JOINT {1}", stab, joint.Name);

            writer.WriteLine(stab + "{");

            stab += "\t";

            writer.WriteLine("{0}OFFSET {1} {2} {3}", stab, joint.Offset.x.ToString(nfi), joint.Offset.y.ToString(nfi), joint.Offset.z.ToString(nfi));
            if (joint.NumberofChannels > 0)
            {
                writer.Write("{0}CHANNELS {1}", stab, joint.NumberofChannels);
                for (int i = 0; i < joint.NumberofChannels; i++)
                    writer.Write(" {0}", Joint.ChannelString(joint.ChannelsOrder[i]));
                writer.WriteLine("");
            }

            foreach(Joint child in joint.Children)
                SaveJoint(writer, child, tab+1);

            stab = "";
            for (int i = 0; i < tab; i++)
                stab += "\t";

            writer.WriteLine(stab + "}");
        }

        /// <summary>
        /// save motion data to bvh format
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="motion"></param>
        private void SaveMotion(StreamWriter writer, Motion motion)
        {
            NumberFormatInfo nfi = new NumberFormatInfo();
            nfi.NumberDecimalSeparator = ".";

            writer.WriteLine("MOTION");
            writer.WriteLine("Frames: {0}", motion.NumFrames);
            writer.WriteLine("Frame Time: {0}", motion.FrameTime.ToString(nfi));
            for(int i = 0; i < motion.NumFrames; i++)
            {
                string s = "";
                int index = i * motion.NumMotionChannels;
                for (int j = 0; j < motion.NumMotionChannels; j++)
                {
                    s += motion.Data[index + j].ToString(nfi) + " ";
                }
                writer.WriteLine(s);
            }
        }
        
        /// <summary>
        /// load hieararchy from bvh file
        /// </summary>
        /// <param name="index"></param>
        private void LoadHierarchy(int index)
        {
            while (tokens[index].MoveNext())
            {
                if (tokens[index].Current.ToString().Trim().StartsWith("ROOT"))
                    motions[index].RootJoint = LoadJoint(index);
                else if (tokens[index].Current.ToString().Trim().StartsWith("MOTION"))
                    LoadMotion(index);
            }
        }

        /// <summary>
        /// load joint structure from bvh file
        /// </summary>
        /// <param name="index"></param>
        /// <param name="parent"></param>
        /// <returns></returns>
        private Joint LoadJoint(int index, Joint parent = null)
        {
            int channelOrderIndex = 0;

            tokens[index].MoveNext();   // ROOT / JOINT / End

            Joint joint = new Joint();
            joint.Name = tokens[index].Current.ToString().Trim();
            joint.Matrix = Matrix4x4.identity;

            while (tokens[index].MoveNext())
            {
                string tmp = tokens[index].Current.ToString().Trim();
                if (tmp.StartsWith("Xposition"))
                {
                    joint.ChannelsOrder[channelOrderIndex++] = Joint.Channel.Xposition;
                }
                if (tmp.StartsWith("Yposition"))
                {
                    joint.ChannelsOrder[channelOrderIndex++] = Joint.Channel.Yposition;
                }
                if (tmp.StartsWith("Zposition"))
                {
                    joint.ChannelsOrder[channelOrderIndex++] = Joint.Channel.Zposition;
                }

                if (tmp.StartsWith("Xrotation"))
                {
                    joint.ChannelsOrder[channelOrderIndex++] = Joint.Channel.Xrotation;
                }
                if (tmp.StartsWith("Yrotation"))
                {
                    joint.ChannelsOrder[channelOrderIndex++] = Joint.Channel.Yrotation;
                }
                if (tmp.StartsWith("Zrotation"))
                {
                    joint.ChannelsOrder[channelOrderIndex++] = Joint.Channel.Zrotation;
                }

                // read OFFSET
                if (tmp.StartsWith("OFFSET"))
                {
                    CultureInfo ci = (CultureInfo)CultureInfo.CurrentCulture.Clone();
                    ci.NumberFormat.CurrencyDecimalSeparator = ".";

                    // reading an offset values
                    tokens[index].MoveNext(); joint.Offset.x = float.Parse(tokens[index].Current.ToString().Trim(), NumberStyles.Any, ci);
                    tokens[index].MoveNext(); joint.Offset.y = float.Parse(tokens[index].Current.ToString().Trim(), NumberStyles.Any, ci);
                    tokens[index].MoveNext(); joint.Offset.z = float.Parse(tokens[index].Current.ToString().Trim(), NumberStyles.Any, ci);
                }
                else if (tmp.StartsWith("CHANNELS"))
                {
                    // loading num of channels
                    tokens[index].MoveNext();
                    joint.NumberofChannels = int.Parse(tokens[index].Current.ToString().Trim());

                    // adding to motiondata
                    motions[index].NumMotionChannels += joint.NumberofChannels;

                    // increasing static counter of channel index starting motion section
                    joint.ChannelStart = motions[index].ChannelStart;
                    motions[index].ChannelStart += joint.NumberofChannels;

                    // creating array for channel order specification
                    joint.ChannelsOrder = new Joint.Channel[joint.NumberofChannels];

                }
                else if (tmp.StartsWith("JOINT"))
                {
                    // loading child joint and setting this as a parent
                    Joint tmpJoint = LoadJoint(index, joint);

                    tmpJoint.Parent = joint;
                    joint.Children.Add(tmpJoint);

                }
                else if (tmp.StartsWith("End"))
                {
                    Joint tmpJoint = LoadJoint(index, joint);

                    tmpJoint.Parent = joint;
                    tmpJoint.NumberofChannels = 0;
                    joint.Children.Add(tmpJoint);

                }
                else if (tmp == "}")
                    return joint;
            }

            return joint;
        }

        /// <summary>
        /// load motion data from bvh file
        /// </summary>
        /// <param name="index"></param>
        private void LoadMotion(int index)
        {
            while (tokens[index].MoveNext())
            {
                string tmp = tokens[index].Current.ToString().Trim();

                if (tmp.StartsWith("Frames:"))
                {
                    tokens[index].MoveNext();
                    // loading frame number
                    motions[index].NumFrames = int.Parse(tokens[index].Current.ToString().Trim());

                }
                else if (tmp.StartsWith("Frame"))
                {
                    tokens[index].MoveNext();   // Time:
                    tokens[index].MoveNext();

                    // loading frame time
                    CultureInfo ci = (CultureInfo)CultureInfo.CurrentCulture.Clone();
                    ci.NumberFormat.CurrencyDecimalSeparator = ".";

                    motions[index].FrameTime = float.Parse(tokens[index].Current.ToString().Trim(), NumberStyles.Any, ci);

                    int numFrames = motions[index].NumFrames;
                    int numChannels = motions[index].NumMotionChannels;

                    // creating motion data array
                    motions[index].Data = new float[numFrames * numChannels];

                    // foreach frame read and store floats
                    for (int frame = 0; frame < numFrames; frame++)
                    {
                        for (int channel = 0; channel < numChannels; channel++)
                        {
                            tokens[index].MoveNext();

                            while (tokens[index].Current.ToString().Trim().Length == 0)
                            {
                                tokens[index].MoveNext();
                            }
                            int idx = frame * numChannels + channel;

                            // calculating index for storage
                            motions[index].Data[idx] = float.Parse(tokens[index].Current.ToString().Trim(), NumberStyles.Any, ci);
                        }
                    }
                }
            }
        }
        
    }
}