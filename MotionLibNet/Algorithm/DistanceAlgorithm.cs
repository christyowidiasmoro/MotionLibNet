﻿/*
 * License here
 * 
 * christyowidiasmoro (c.christyowidiasmoro@uu.nl, christyowidiasmoro@gmail.com)
 */

using System.Collections.Generic;

namespace MotionLibNet.Algorithm
{
    /// <summary>
    /// abstract class for generic distance algorithm
    /// </summary>
    public abstract class DistanceAlgorithm
    {
        /// <summary>
        /// implement this function to calculate specific distance from pair of point set
        /// </summary>
        /// <param name="p1"></param>
        /// <param name="p2"></param>
        /// <returns></returns>
        public abstract float Calculate(List<Vector3> p1, List<Vector3> p2);

        /// <summary>
        /// implement this function to calculate distance param only, without returning the distance
        /// </summary>
        /// <param name="p1"></param>
        /// <param name="p2"></param>
        /// <returns></returns>
        public abstract void CalculateParamOnly(List<Vector3> p1, List<Vector3> p2);

        /// <summary>
        /// implement this function to calculate position from last calculated distance param
        /// </summary>
        /// <returns></returns>
        public abstract Vector3 CalculatePositionFromParam(Vector3 oldPosition);

        /// <summary>
        /// print param for debugging purpose
        /// </summary>
        /// <returns></returns>
        public abstract string PrintParam();

    }
}
