﻿/*
 * License here
 * 
 * christyowidiasmoro (c.christyowidiasmoro@uu.nl, christyowidiasmoro@gmail.com)
 */

using System;
using System.Collections.Generic;
using System.Text;
using QuickGraph;
using QuickGraph.Algorithms;
using QuickGraph.Algorithms.RandomWalks;
using QuickGraph.Graphviz;
using MotionLibNet.Util;
using MotionLibNet.IO;

namespace MotionLibNet.Algorithm.MotionGraphs
{
    /// <summary>
    /// Implementation of motion graph algorithm 
    /// Kovar, Lucas, Michael Gleicher, and Frédéric Pighin. "Motion graphs." ACM transactions on graphics (TOG). Vol. 21. No. 3. ACM, 2002.
    /// </summary>
    public class MotionGraphs
    {
        /// <summary>
        /// 
        /// </summary>
        [Serializable]
        public class MotionGraphNode
        {
            /// <summary>
            /// 
            /// </summary>
            public string MotionName;

            /// <summary>
            /// 
            /// </summary>
            public int Frame;

            /// <summary>
            /// 
            /// </summary>
            public int ColorIndex;

            /// <summary>
            /// 
            /// </summary>
            /// <returns></returns>
            public override string ToString()
            {
                return MotionName + " " + Frame;
            }

            /// <summary>
            /// needed to compare this object
            /// </summary>
            /// <returns></returns>
            public override int GetHashCode()
            {
                if (MotionName == null) return 0;
                return MotionName.GetHashCode() ^ Frame.GetHashCode();
            }

            /// <summary>
            /// needed to compare this object
            /// </summary>
            /// <param name="obj"></param>
            /// <returns></returns>
            public override bool Equals(object obj)
            {
                bool result;
                if (!(obj is MotionGraphNode))
                {
                    result = false;
                }
                else
                {
                    MotionGraphNode node = (MotionGraphNode)obj;
                    result = MotionName.Equals(node.MotionName) && Frame.Equals(node.Frame);
                }
                return result;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [Serializable]
        public class Transition
        {
            /// <summary>
            /// 
            /// </summary>
            public string[] MotionName;

            /// <summary>
            /// 
            /// </summary>
            public int[] Frame;
        }

        /// <summary>
        /// 
        /// </summary>
        [Serializable]
        public class Pair
        {
            /// <summary>
            /// two dimensional array to save the motion name from two frames
            /// </summary>
            public string[] MotionName;

            /// <summary>
            /// two dimensional array to save the frame number from two frames
            /// </summary>
            public int[] FrameNumbers;

            /// <summary>
            /// store the distances from this two frames
            /// </summary>
            public float[,] Distances;

            /// <summary>
            /// 
            /// </summary>
            public List<Vector2> LocalMinima;
            //public List<Transition> transitions = new List<Transition>();
        }

        private DistanceAlgorithm distanceAlgorithm;

        /// <summary>
        /// 
        /// </summary>
        private List<Pair> distancePair = new List<Pair>();

        /// <summary>
        /// 
        /// </summary>
        private Dictionary<string, Motion> motionDatabase = new Dictionary<string, Motion>();

        /// <summary>
        /// 
        /// </summary>
        private float defaultCost = 1.0f;

        /// <summary>
        /// 
        /// </summary>
        private BidirectionalGraph<MotionGraphNode, TaggedEdge<MotionGraphNode, float>> graph = new BidirectionalGraph<MotionGraphNode, TaggedEdge<MotionGraphNode, float>>();

        /// <summary>
        /// 
        /// </summary>
        private BidirectionalGraph<MotionGraphNode, TaggedEdge<MotionGraphNode, float>> lastWalkGraph = new BidirectionalGraph<MotionGraphNode, TaggedEdge<MotionGraphNode, float>>();
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="distance"></param>
        public MotionGraphs(DistanceAlgorithm distance)
        {
            distanceAlgorithm = distance;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="motion1"></param>
        /// <param name="motion2"></param>
        public void DetectCandidateTransition(Motion motion1, Motion motion2)
        {
            System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();

            UpdateDatabase(motion1);
            UpdateDatabase(motion2);

            Pair pair = new Pair();
            pair.MotionName = new string[] { motion1.Name, motion2.Name };
            pair.FrameNumbers = new int[] { motion1.NumFrames, motion2.NumFrames };
            pair.Distances = new float[pair.FrameNumbers[0], pair.FrameNumbers[1]];

            for (int i = 0; i < motion1.NumFrames; i++)
            {
                motion1.MoveTo(i);
                List<Vector3> positions1 = new List<Vector3>();
                motion1.RootJoint.ExtractJointPosition(ref positions1);

                for (int j = 0; j < motion2.NumFrames; j++)
                {
                    motion2.MoveTo(j);
                    List<Vector3> positions2 = new List<Vector3>();
                    motion2.RootJoint.ExtractJointPosition(ref positions2);

                    pair.Distances[i, j] = distanceAlgorithm.Calculate(positions1, positions2);
                }
            }

            pair.LocalMinima = new List<Vector2>();
            FindLocalMinima(pair.Distances, 0, 0, pair.FrameNumbers[0], pair.FrameNumbers[1], ref pair.LocalMinima);

            distancePair.Add(pair);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="threshold"></param>
        public void CreateGraph(float threshold)
        {
            foreach (KeyValuePair<string, Motion> motion in motionDatabase)
            {
                MotionGraphNode oldNode = null;
                for (int i = 0; i < motion.Value.NumFrames; i++)
                {
                    MotionGraphNode node = new MotionGraphNode { MotionName = motion.Key, Frame = i };
                    //motion.Value.MoveTo(i, out node.RootJoint);
                    graph.AddVertex(node);

                    if (oldNode != null)
                    {
                        //graph.AddDirectedEdge(oldNode, node, defaultCost);
                        graph.AddEdge(new TaggedEdge<MotionGraphNode, float>(oldNode, node, defaultCost));
                    }

                    oldNode = node;
                }
            }

            foreach (Pair pair in distancePair)
            {
                foreach (Vector2 vec in pair.LocalMinima)
                {
                    int x = (int)vec.x;
                    int y = (int)vec.y;

                    if (pair.Distances[x, y] <= threshold)
                    {
                        MotionGraphNode node1 = new MotionGraphNode { MotionName = pair.MotionName[0], Frame = x };
                        //motionDatabase[pair.MotionName[0]].MoveTo(i, out node1.RootJoint);

                        MotionGraphNode node2 = new MotionGraphNode { MotionName = pair.MotionName[1], Frame = y };
                        //motionDatabase[pair.MotionName[0]].MoveTo(j, out node2.RootJoint);

                        if (graph.ContainsEdge(node1, node2))
                            continue;

                        if (graph.ContainsEdge(node2, node1))
                            continue;

                        // skip for loop vertex
                        if (node1.Equals(node2))
                            continue;

                        Console.WriteLine("Edge " + node1.Frame + " => " + node2.Frame);

                        graph.AddEdge(new TaggedEdge<MotionGraphNode, float>(node1, node2, defaultCost));
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void PruneGraph()
        {
            StronglyConnectedComponentsAlgorithm<MotionGraphNode, TaggedEdge<MotionGraphNode, float>> scc =
                new StronglyConnectedComponentsAlgorithm<MotionGraphNode, TaggedEdge<MotionGraphNode, float>>(graph);

            scc.Compute();

            if (scc.ComponentCount != 1) // this graph has multiple components
            {
                Console.WriteLine("Graph contains {0} strongly connected components", scc.ComponentCount);

                List<MotionGraphNode>[] clusters = new List<MotionGraphNode>[scc.ComponentCount];
                foreach (var kv in scc.Components)
                {
                    //Console.WriteLine("Vertex {0} is connected to subgraph {1}", kv.Key, kv.Value);

                    if (clusters[kv.Value] == null)
                        clusters[kv.Value] = new List<MotionGraphNode>();

                    clusters[kv.Value].Add(kv.Key);
                }

                // find largest scc
                int largestIndex = 0;
                for (int i = 0; i < clusters.Length; i++)
                {
                    string s = "";
                    foreach (MotionGraphNode node in clusters[i])
                        s += node.MotionName + " " + node.Frame;

                    // Console.WriteLine("cluster " + i + "\n" + s);

                    if (clusters[largestIndex].Count < clusters[i].Count)
                        largestIndex = i;
                }
                Console.WriteLine("largest cluster " + largestIndex + " " + clusters[largestIndex].Count);

                // TODO eliminate any edge that doesn't attach two nodes in the largest scc
                Console.WriteLine("total vertex = {0}\ntotal edges = {1}", graph.VertexCount, graph.EdgeCount);

                graph.RemoveEdgeIf(new EdgePredicate<MotionGraphNode, TaggedEdge<MotionGraphNode, float>>((e) =>
                {
                    int i = 0;
                    foreach (MotionGraphNode node in clusters[largestIndex])
                    {
                        if (e.Source.Equals(node))
                            i++;

                        if (e.Target.Equals(node))
                            i++;


                        if (i >= 2)
                        {
                            //Console.WriteLine("predicate " + e.Source + " " + e.Target + " " + node.MotionName);
                            return false;
                        }
                    }

                    return true;
                }));
                Console.WriteLine("total vertex = {0}\ntotal edges = {1}", graph.VertexCount, graph.EdgeCount);

                // TODO eliminate any node without edge attach to it
                graph.RemoveVertexIf(new VertexPredicate<MotionGraphNode>((v) =>
                {
                    if (graph.InDegree(v) == 0 && graph.OutDegree(v) == 0)
                        return true;
                    else
                        return false;
                }));

                Console.WriteLine("total vertex = {0}\ntotal edges = {1}", graph.VertexCount, graph.EdgeCount);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="motion"></param>
        /// <param name="blendingFrame"></param>
        /// <param name="walkCount"></param>
        /// <param name="reset"></param>
        public void RandomWalk(ref Motion motion, int blendingFrame, int walkCount = 100, bool reset = false)
        {
            MotionGraphNode root = null;

            IEnumerator<MotionGraphNode> e = graph.Vertices.GetEnumerator();

            // random start
            // continue our lastwalkgraph
            if (lastWalkGraph.VertexCount > 0 && !reset)
            {
                Console.Out.WriteLine("continue last walk graph");
                IEnumerator<MotionGraphNode> v = lastWalkGraph.Vertices.GetEnumerator();
                while (v.MoveNext())
                    root = v.Current;
            }
            else if (motion == null || reset)
            {
                Console.Out.WriteLine("random a new walk graph");
                Random rnd = new Random();
                int startIndex = rnd.Next(0, graph.VertexCount);
                while (startIndex > 0 && e.MoveNext())
                    startIndex--;
                root = e.Current;
            }
            else if (motion != null)
            {
                Console.Out.WriteLine("finding the match motion inside the graph");
                List<Vector3> positions1 = new List<Vector3>();
                motion.RootJoint.ExtractJointPosition(ref positions1);
                float minDistance = float.MaxValue;

                while (e.MoveNext())
                {
                    Motion motion2 = motionDatabase[e.Current.MotionName];
                    motion2.MoveTo(e.Current.Frame);

                    List<Vector3> positions2 = new List<Vector3>();
                    motion2.RootJoint.ExtractJointPosition(ref positions2);

                    float d = distanceAlgorithm.Calculate(positions1, positions2);

                    if (d < minDistance)
                    {
                        root = e.Current;
                        minDistance = d;
                    }
                }
            }

            lastWalkGraph.Clear();

            RandomWalkAlgorithm<MotionGraphNode, TaggedEdge<MotionGraphNode, float>> walk =
                new RandomWalkAlgorithm<MotionGraphNode, TaggedEdge<MotionGraphNode, float>>(
                    graph,
                    new NoLoopMarkovEdgeChain<MotionGraphNode, TaggedEdge<MotionGraphNode, float>>());
            walk.TreeEdge += Walk_TreeEdge;
            walk.Generate(root, 100);

            ApplyTransitionBlending(ref motion, blendingFrame);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="result"></param>
        /// <param name="k"></param>
        public void ApplyTransitionBlending(ref Motion result, int k)
        {
            MotionGraphNode oldNode = null;
            float theta = 0, x0 = 0, z0 = 0;

            // force to calculate a new theta, x0, and z0
            if (result != null)
                oldNode = new MotionGraphNode { MotionName = "oldMotion", Frame = -1 };

            foreach (MotionGraphNode node in lastWalkGraph.Vertices)
            {
                Motion motion = motionDatabase[node.MotionName];
                motion.MoveTo(node.Frame);

                Console.Out.WriteLine("motion {0} : {1}", motion.Name, node.Frame);

                if (result == null)
                {
                    result = motion.CopyStructure();
                    result.Name = "RandomWalkMotion";
                    result.RecordTo(0, motion.RootJoint);
                    result.NumFrames = 1;
                }
                else
                {
                    if ((!node.MotionName.Equals(oldNode.MotionName)) ||
                        (node.MotionName.Equals(oldNode.MotionName) && Mathf.Abs(node.Frame - oldNode.Frame) > 1))
                    {
                        result.MoveTo(result.NumFrames - 1);

                        List<Vector3> pA = new List<Vector3>();
                        result.RootJoint.ExtractJointPosition(ref pA);

                        List<Vector3> pB = new List<Vector3>();
                        motion.RootJoint.ExtractJointPosition(ref pB);

                        distanceAlgorithm.CalculateParamOnly(pA, pB);

                        Console.Out.Write("calculate 2D Transform = {0} {1} {2}", theta, x0, z0);
                    }

                    result.RootJoint = motion.RootJoint.Clone();

                    result.RootJoint.Position = distanceAlgorithm.CalculatePositionFromParam(motion.RootJoint.Position);

                    /*
                     * TODO:
                    // blending
                    float a1 = (p + 1) / result.NumFrames;
                    float alpha = 2 * a1 * a1 * a1 - 3 * a1 * a1 + 1;
                    t.rootPosition.Add(alpha * Aip.RootJoint.Position + (1 - alpha) * Bjkp.RootJoint.Position);

                    List<Quaternion> q = new List<Quaternion>();
                    for (int i = 0; i < Aip.jointOrientations.Count; i++)
                    {
                        q.Add(Quaternion.Slerp(Aip.jointOrientations[i], Bjkp.jointOrientations[i], alpha));
                    }
                    */

                    result.RecordTo(result.NumFrames, result.RootJoint);
                    result.NumFrames++;
                }

                oldNode = node;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, Motion> GetMotionDatabase()
        {
            return motionDatabase;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="motion"></param>
        public void UpdateDatabase(Motion motion)
        {
            if (!motionDatabase.ContainsKey(motion.Name))
                motionDatabase.Add(motion.Name, motion);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public Pair GetDistancePair(int index)
        {
            return distancePair[index];
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="index"></param>
        /// <param name="pair"></param>
        public void SetDistancePair(int index, Pair pair)
        {
            if (index < distancePair.Count)
            {
                distancePair[index] = pair;
            }
            else
            {
                distancePair.Add(pair);
            }
        }

        public DistanceAlgorithm getDistanceAlgorithm()
        {
            return distanceAlgorithm;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public string GraphToDot(string filename)
        {
            GraphDot<MotionGraphNode> graphDot = new GraphDot<MotionGraphNode>();

            graphDot.VertexColoring += GraphDot_VertexColoring;
            return graphDot.GraphToDot(filename, graph);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public string RandomWalkGraphToDot(string filename)
        {
            GraphDot<MotionGraphNode> graphDot = new GraphDot<MotionGraphNode>();

            graphDot.VertexColoring += GraphDot_VertexColoring;
            return graphDot.GraphToDot(filename, lastWalkGraph);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filename"></param>
        public void SaveGraph(string filename)
        {
            FileManager.SerializeToFile(filename, graph);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filename"></param>
        public void SaveRandomWalkGraph(string filename)
        {
            FileManager.SerializeToFile(filename, this.lastWalkGraph);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filename"></param>
        public void LoadGraph(string filename)
        {
            FileManager.DeserializeFromFile<BidirectionalGraph<MotionGraphNode, TaggedEdge<MotionGraphNode, float>>>(filename, out graph);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filename"></param>
        public void LoadRandomWalkGraph(string filename)
        {
            FileManager.DeserializeFromFile<BidirectionalGraph<MotionGraphNode, TaggedEdge<MotionGraphNode, float>>>(filename, out lastWalkGraph);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="values"></param>
        /// <param name="start1"></param>
        /// <param name="start2"></param>
        /// <param name="length1"></param>
        /// <param name="length2"></param>
        /// <param name="minimums"></param>
        private void FindLocalMinima(float[,] values, int start1, int start2, int length1, int length2, ref List<Vector2> minimums)
        {
            if (length1 <= 0 || length2 <= 0)
                return;

            if (length1 < 2 && length2 < 2)
            {
                float tmp = float.MaxValue;
                int tmp1 = start1, tmp2 = start2;
                for (int i = start1; i < start1 + length1; i++)
                {
                    for (int j = start2; j < start2 + length2; j++)
                    {
                        if (values[i, j] < tmp)
                        {
                            tmp = values[i, j];
                            tmp1 = i; tmp2 = j;
                        }
                    }
                }
                minimums.Add(new Vector2(tmp1, tmp2));
            }
            else
            {
                int l1 = length1 / 2;
                int l2 = length2 / 2;

                List<Vector2> mins1 = new List<Vector2>();
                FindLocalMinima(values, start1, start2, l1, l2, ref mins1);
                mins1.RemoveAll(new Predicate<Vector2>((v) => {
                    int x = (int)v.x;
                    int y = (int)v.y;

                    if (x == start1 + l1 - 1 && x < values.GetLength(0) && values[x, y] > values[x + 1, y])
                        return true;
                    else if (y == start2 + l2 - 1 && y < values.GetLength(1) && values[x, y] > values[x, y + 1])
                        return true;
                    return false;
                }));
                minimums.AddRange(mins1);

                List<Vector2> mins2 = new List<Vector2>();
                FindLocalMinima(values, start1 + l1, start2, length1 - l1, l2, ref mins2);
                mins2.RemoveAll(new Predicate<Vector2>((v) => {
                    int x = (int)v.x;
                    int y = (int)v.y;

                    if (x == start1 + l1 && x > 0 && values[x, y] > values[x - 1, y])
                        return true;
                    else if (y == start2 + l2 - 1 && y < values.GetLength(1) && values[x, y] > values[x, y + 1])
                        return true;
                    return false;
                }));
                minimums.AddRange(mins2);

                List<Vector2> mins3 = new List<Vector2>();
                FindLocalMinima(values, start1, start2 + l2, l1, length2 - l2, ref mins3);
                mins3.RemoveAll(new Predicate<Vector2>((v) => {
                    int x = (int)v.x;
                    int y = (int)v.y;

                    if (x == start1 + l1 - 1 && x < values.GetLength(0) && values[x, y] > values[x + 1, y])
                        return true;
                    else if (y == start2 + l2 && y > 0 && values[x, y] > values[x, y - 1])
                        return true;
                    return false;
                }));
                minimums.AddRange(mins3);

                List<Vector2> mins4 = new List<Vector2>();
                FindLocalMinima(values, start1 + l1, start2 + l2, length1 - l1, length2 - l2, ref mins4);
                mins4.RemoveAll(new Predicate<Vector2>((v) => {
                    int x = (int)v.x;
                    int y = (int)v.y;

                    if (x == start1 + l1 && x > 0 && values[x, y] > values[x - 1, y])
                        return true;

                    else if (y == start2 + l2 && y > 0 && values[x, y] > values[x, y - 1])
                        return true;

                    return false;
                }));
                minimums.AddRange(mins4);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Walk_TreeEdge(object sender, EdgeEventArgs<MotionGraphNode, TaggedEdge<MotionGraphNode, float>> e)
        {
            System.Console.WriteLine(string.Format("{0} -> {1}", e.Edge.Source, e.Edge.Target));

            MotionGraphNode node1 = e.Edge.Source;
            MotionGraphNode node2 = e.Edge.Target;

            float[] n1 = new float[motionDatabase[node1.MotionName].NumMotionChannels];
            float[] n2 = new float[motionDatabase[node2.MotionName].NumMotionChannels];
            int start1 = node1.Frame * motionDatabase[node1.MotionName].NumMotionChannels;
            int start2 = node2.Frame * motionDatabase[node2.MotionName].NumMotionChannels;

            Array.Copy(motionDatabase[node1.MotionName].Data, start1, n1, 0, motionDatabase[node1.MotionName].NumMotionChannels);
            Array.Copy(motionDatabase[node2.MotionName].Data, start2, n2, 0, motionDatabase[node2.MotionName].NumMotionChannels);

            if (!lastWalkGraph.ContainsVertex(node1))
                lastWalkGraph.AddVertex(node1);

            if (!lastWalkGraph.ContainsVertex(node2))
                lastWalkGraph.AddVertex(node2);

            if (!lastWalkGraph.ContainsEdge(e.Edge))
                lastWalkGraph.AddEdge(e.Edge);

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="node"></param>
        /// <param name="oldNode"></param>
        /// <param name="lastIndex"></param>
        /// <returns></returns>
        private int GraphDot_VertexColoring(MotionGraphNode node, MotionGraphNode oldNode, ref int lastIndex)
        {
            if (!node.MotionName.Equals(oldNode.MotionName))
                lastIndex++;

            return lastIndex;
        }



        /*
        private void CreatingTransitions(ref Motion result, Motion motion1, Motion motion2, int k)
        {
            for (int p = 0; p < result.NumFrames; p++)
            {
                // alpha = -1, if p <= -1
                // alpha = 0, if p >= k
                float a1 = (p + 1) / result.NumFrames;
                float alpha = 2 * a1 * a1 * a1 - 3 * a1 * a1 + 1;

                int indexA = (int)t.frameIndex[0] + p;
                if (indexA < 0) indexA = 0;
                if (indexA >= motionDatabase[motion1.Name].NumFrames) indexA = motionDatabase[motion1.Name].NumFrames - 1;

                int indexB = (int)t.frameIndex[1] - k + 1 + p;
                if (indexB < 0) indexB = 0;
                if (indexB >= motionDatabase[motion2.Name].NumFrames) indexA = motionDatabase[motion2.Name].NumFrames - 1;

                Motion Aip = motionDatabase[motion1.Name].frames[indexA];
                Motion Bjkp = motionDatabase[motion2.Name].frames[indexB];

                float a1 = (p + 1) / result.NumFrames;
                float alpha = 2 * a1 * a1 * a1 - 3 * a1 * a1 + 1;
                t.rootPosition.Add(alpha * Aip.RootJoint.Position + (1 - alpha) * Bjkp.RootJoint.Position);

                List<Quaternion> q = new List<Quaternion>();
                for (int i = 0; i < Aip.jointOrientations.Count; i++)
                {
                    q.Add(Quaternion.Slerp(Aip.jointOrientations[i], Bjkp.jointOrientations[i], alpha));
                }
                t.jointOrientation.Add(q);
            }
        }
        */

    }
}
