﻿/*
 * License here
 * 
 * christyowidiasmoro (c.christyowidiasmoro@uu.nl, christyowidiasmoro@gmail.com)
 */

using System;
using System.Collections.Generic;
using MotionLibNet.Util;

namespace MotionLibNet.Algorithm.MotionGraphs
{
    /// <summary>
    /// class to calculate distance based on motion graph paper
    /// </summary>
    public class MotionGraphsDistance : DistanceAlgorithm
    {

        /// <summary>
        /// 
        /// </summary>
        private float theta;

        /// <summary>
        /// 
        /// </summary>
        private float x0;

        /// <summary>
        /// 
        /// </summary>
        private float z0;


        /// <summary>
        /// calculate distance based from motion graph paper
        /// </summary>
        /// <param name="p1"></param>
        /// <param name="p2"></param>
        /// <returns></returns>
        public override float Calculate(List<Vector3> p1, List<Vector3> p2)
        {
            if (p1.Count != p2.Count)
                return 0;

            List<float> w = GenerateJointWeight();

            List<Vector3> p3;
            Calculate2DTransform(p1, p2, w, out p3);

            float sum = 0;
            for (int i = 0; i < p1.Count; i++)
                sum += w[i] * (p1[i] - p3[i]).sqrMagnitude;

            return sum;
        }

        /// <summary>
        /// implement this function to calculate distance param only, without returning the distance
        /// </summary>
        /// <param name="p1"></param>
        /// <param name="p2"></param>
        /// <returns></returns>
        public override void CalculateParamOnly(List<Vector3> p1, List<Vector3> p2)
        {
            Calculate2DTransformParam(p1, p2, GenerateJointWeight(), out theta, out x0, out z0);
        }

        /// <summary>
        /// implement this function to calculate position from last calculated distance param
        /// </summary>
        /// <returns></returns>
        public override Vector3 CalculatePositionFromParam(Vector3 oldPosition)
        {
            return (Quaternion.AngleAxis(theta, Vector3.up) * oldPosition) +
                new Vector3(x0, 0, z0);

        }

        /// <summary>
        /// print param for debugging purpose
        /// </summary>
        /// <returns></returns>
        public override string PrintParam()
        {
            return string.Format("Theta = {0}, x0 = {1}, z0 = {2} ", theta, x0, z0);
        }

        /// <summary>
        /// TODO: need to tune this one!
        /// </summary>
        /// <returns></returns>
        private List<float> GenerateJointWeight()
        {
            List<float> w = new List<float>();

            w.Add(1);   // pelvis
            w.Add(1);   // lfemur
            w.Add(1);   // ltibia
            w.Add(1);   // lfoot
            w.Add(1);   // ltoes
            w.Add(1);   // end site
            w.Add(1);   // rfemur
            w.Add(1);   // rtibia
            w.Add(1);   // rfoot
            w.Add(1);   // rtoes
            w.Add(1);   // end site
            w.Add(1);   // thorax
            w.Add(1);   // head
            w.Add(1);   // end site
            w.Add(1);   // lclavicle
            w.Add(1);   // lhumerus
            w.Add(1);   // lradius
            w.Add(1);   // lhand
            w.Add(1);   // end site
            w.Add(1);   // rclavicle
            w.Add(1);   // rhumerus
            w.Add(1);   // rradius
            w.Add(1);   // rhand
            w.Add(1);   // end site

            return w;
        }

        /// <summary>
        /// translate p2 to p1 and rotate around y-axis
        /// </summary>
        /// <param name="p1"></param>
        /// <param name="p2"></param>
        /// <param name="w"></param>
        /// <param name="p3"></param>
        private void Calculate2DTransform(List<Vector3> p1, List<Vector3> p2, List<float> w, out List<Vector3> p3)
        {
            float theta, x0, z0;
            Calculate2DTransformParam(p1, p2, w, out theta, out x0, out z0);

            p3 = new List<Vector3>();

            for (int i = 0; i < p1.Count; i++)
                p3.Add((Quaternion.AngleAxis(theta, Vector3.up) * p2[i]) + new Vector3(x0, 0, z0));
        }

        /// <summary>
        /// calculate theta, x0 and z0 that required to translate p2 to p1 and rotate around y-axis
        /// </summary>
        /// <param name="p1"></param>
        /// <param name="p2"></param>
        /// <param name="w"></param>
        /// <param name="theta"></param>
        /// <param name="x0"></param>
        /// <param name="z0"></param>
        private void Calculate2DTransformParam(List<Vector3> p1, List<Vector3> p2, List<float> w, out float theta, out float x0, out float z0)
        {
            float sum1 = 0;
            for (int i = 0; i < w.Count; i++)
                sum1 += w[i] * ((p1[i].x * p2[i].z) - (p2[i].x * p1[i].z));

            float sum2 = 0;
            for (int i = 0; i < w.Count; i++)
                sum2 += w[i] * ((p1[i].x * p2[i].x) + (p1[i].z * p2[i].z));

            float sumW = 0;
            for (int i = 0; i < w.Count; i++)
                sumW += w[i];

            float sumP1x = 0;
            for (int i = 0; i < w.Count; i++)
                sumP1x += w[i] * p1[i].x;

            float sumP1z = 0;
            for (int i = 0; i < w.Count; i++)
                sumP1z += w[i] * p1[i].z;

            float sumP2x = 0;
            for (int i = 0; i < w.Count; i++)
                sumP2x += w[i] * p2[i].x;

            float sumP2z = 0;
            for (int i = 0; i < w.Count; i++)
                sumP2z += w[i] * p2[i].z;


            float t1 = (sum1 - ((1 / sumW) * ((sumP1x * sumP2z) - (sumP2x * sumP1z))));
            float t2 = (sum2 - ((1 / sumW) * ((sumP1x * sumP2x) - (sumP1z * sumP2z))));

            theta = Mathf.Atan(t1 / t2);

            x0 = (1 / sumW) * (sumP1x - (sumP2x * Mathf.Cos(theta)) - (sumP2z * Mathf.Sin(theta)));

            z0 = (1 / sumW) * (sumP1z + (sumP2x * Mathf.Sin(theta)) - (sumP2z * Mathf.Cos(theta)));
        }
    }
}
