﻿/*
 * License here
 * 
 * christyowidiasmoro (c.christyowidiasmoro@uu.nl, christyowidiasmoro@gmail.com)
 */

using System;
using System.Collections.Generic;
using MotionLibNet.Util;

namespace MotionLibNet
{
    /// <summary>
    /// TODO: not finished yet
    /// </summary>
    public class MotionFields
    {
        /*
            A pose x = (xroot,p0,p1,...,pn) consists of a 3d root position vector xroot, a root orientation quaternion p0 and joint orientation quaternions p1,...pn
            velocity v = (vroot,q0,q1,...,qn) consists of a 3d root displacement vector vroot, root displacement quaternion q0, and joint displacement quaternions q1,...,qn
            mi = (xi,vi) 
        */

        public struct MotionState
        {
            public int index;
            public Pose pose;               // x
            public Velocity velocity;       // v
            public Velocity nextVelocity;   // y

            public List<KeyValuePair<int, float>> neighborhood;
            public Dictionary<int, float> similarityWeight;
        }

        public struct Pose
        {
            public Vector3 rootPosition;            // root position
            public List<Quaternion> jointRotation;  // joint quaternion
        }

        public struct Velocity
        {
            public Vector3 rootDisplacement;            // root displacement vector
            public List<Quaternion> jointDisplacement;  // joint displacement quaternion
        }

        private List<float> boneLength = new List<float>();

        //private List<Pose> pose = new List<Pose>();
        //private List<Velocity> velocity = new List<Velocity>();

        private Dictionary<string, MotionState> motionDatabase = new Dictionary<string, MotionState>();

        private List<MotionState> motionStates = new List<MotionState>();

        private float[,] dissimilarities = null;

        public void AddMotionData(Motion motion)
        {
            if (boneLength.Count == 0)
            {
                motion.MoveTo(0);
                motion.RootJoint.ExtractBoneLength(ref boneLength);
                Console.Write("boneLength = ");
                foreach(float b in boneLength)
                {
                    Console.Out.Write(" " + b);
                }
                Console.WriteLine("");
            }

            int startIndex = motionStates.Count;
            for (int i = 0; i < motion.NumFrames; i++)
            {
                motionStates.Add(new MotionState {
                    index = motionStates.Count,
                    neighborhood = new List<KeyValuePair<int, float>>(),
                    similarityWeight = new Dictionary<int, float>()
                });
            }

            for (int i = 0; i < motion.NumFrames; i++)
            {
                MotionState m = motionStates[startIndex + i];
                m.pose.jointRotation = new List<Quaternion>();

                motion.MoveTo(i);
                motion.RootJoint.ExtractJointRotation(ref m.pose.jointRotation);
                m.pose.rootPosition = motion.RootJoint.Position;

                motionStates[startIndex + i] = m;
            }

            int min = 0;
            if (startIndex > 0)
                min = -2;

            for (int i = min; i < motion.NumFrames - 2; i++)
            {
                MotionState m = motionStates[startIndex + i];
                m.velocity.jointDisplacement = new List<Quaternion>();
                m.nextVelocity.jointDisplacement = new List<Quaternion>();

                MotionState m2 = motionStates[startIndex + i + 1];
                MotionState m3 = motionStates[startIndex + i + 2];

                m.velocity.rootDisplacement = m2.pose.rootPosition - m.pose.rootPosition;
                for (int j = 0; j < m.pose.jointRotation.Count; j++)
                {
                    m.velocity.jointDisplacement.Add(m2.pose.jointRotation[j] * Quaternion.Inverse(m.pose.jointRotation[j]));

                    m.nextVelocity.jointDisplacement.Add(m3.pose.jointRotation[j] * Quaternion.Inverse(m2.pose.jointRotation[j]));
                }

                motionStates[startIndex + i] = m;
            }

            int oldLength = 0;
            if (dissimilarities == null)
            {
                dissimilarities = new float[motionStates.Count, motionStates.Count];
            }
            else if (dissimilarities.GetLength(0) != motionStates.Count)
            {
                oldLength = dissimilarities.GetLength(0);
                dissimilarities = ResizeArray<float>(dissimilarities, motionStates.Count, motionStates.Count);
            }

            for (int i = 0; i < oldLength; i++)
            {
                Console.Out.Write("neighbor {0} = ", i);
                for (int j = oldLength; j < motionStates.Count - 2; j++)
                {
                    dissimilarities[i, j] = CalculateDissimilarity(motionStates[i], motionStates[2]);
                    motionStates[i].neighborhood.Add(new KeyValuePair<int, float>(j, dissimilarities[i, j]));
                    Console.Out.Write(" {0} = {1}", j, dissimilarities[i, j]);
                }
                Console.Out.WriteLine("");

                motionStates[i].neighborhood.Sort(delegate (KeyValuePair<int, float> firstPair, KeyValuePair<int, float> nextPair)
                {
                    return firstPair.Value.CompareTo(nextPair.Value);
                });

                CalculateSimilarityWeight(motionStates[i]);
            }

            for (int i = oldLength; i < motionStates.Count - 2; i++)
            {
                for (int j = 0; j < motionStates.Count - 2; j++)
                {
                    dissimilarities[i, j] = CalculateDissimilarity(motionStates[i], motionStates[2]);
                    motionStates[i].neighborhood.Add(new KeyValuePair<int, float>(j, dissimilarities[i, j]));
                }

                motionStates[i].neighborhood.Sort(delegate (KeyValuePair<int, float> firstPair, KeyValuePair<int, float> nextPair)
                {
                    return firstPair.Value.CompareTo(nextPair.Value);
                });

                CalculateSimilarityWeight(motionStates[i]);
            }

            for (int i = 0; i < motionStates.Count; i++)
            {
                Console.Out.WriteLine("neighbor {0} = " + motionStates[i].neighborhood.Count);
            }

            for (int i = 0; i < motionStates.Count - 2; i++)
            {
                CalculateActions(motionStates[i], motionStates[i]);
            }

        }

        private void CalculateActions(MotionState m, MotionState m2)
        {
            float delta = 0.0f;
            int k = 15;

            List<float> a = new List<float>();

            Console.Out.Write("a = ");
            for (int i = 0; i < k; i++)
            {
                float sum = 0;
                for (int j = 0; j < k; j++)
                {
                    if (i == j)
                        sum += 1;
                    else
                        sum += m.neighborhood[j].Value;                
                }
                a.Add(1 / sum);
                Console.Out.Write(" " + a[a.Count-1]);
            }
            Console.Out.WriteLine("");

            Velocity v, y;
            v.jointDisplacement = new List<Quaternion>();
            y.jointDisplacement = new List<Quaternion>();


            Vector3 avRoot = Vector3.zero;
            Vector3 ayRoot = Vector3.zero;
            for (int i = 0; i < k; i++) {
                int idx = m.neighborhood[i].Key;

                avRoot += (a[i] * motionStates[idx].velocity.rootDisplacement);
                ayRoot += (a[i] * motionStates[idx].nextVelocity.rootDisplacement);

            }
            v.rootDisplacement = ((1 - delta) * avRoot) + (delta * m2.velocity.rootDisplacement);
            y.rootDisplacement = ((1 - delta) * ayRoot) + (delta * m2.nextVelocity.rootDisplacement);

            for(int j = 0; j < m.velocity.jointDisplacement.Count; j++)
            {
                Quaternion avJoint = Quaternion.identity;
                Quaternion ayJoint = Quaternion.identity;
                for (int i = 0; i < k; i++)
                {
                    int idx = m.neighborhood[i].Key;

                    avJoint *= (new Quaternion(0, 0, 0, a[i]) * motionStates[idx].velocity.jointDisplacement[j]);
                    ayJoint *= (new Quaternion(0, 0, 0, a[i]) * motionStates[idx].nextVelocity.jointDisplacement[j]);
                }

                v.jointDisplacement.Add(
                    (new Quaternion(0, 0, 0, (1 - delta)) * avJoint) *
                    (new Quaternion(0, 0, 0, delta) *
                    ((m2.pose.jointRotation[j] * m2.velocity.jointDisplacement[j]) * Quaternion.Inverse(m.pose.jointRotation[j]))));

                y.jointDisplacement.Add((new Quaternion(0, 0, 0, (1 - delta)) * ayJoint) * (new Quaternion(0, 0, 0, delta) * m2.nextVelocity.jointDisplacement[j]));
            }
        }

        /// <summary>
        /// 3 Motion Fields
        /// 3.1 Preliminary Deﬁnitions
        /// Similarity and neighborhoods 
        /// </summary>
        /// <param name="m1"></param>
        /// <param name="m2"></param>
        /// <returns></returns>
        private float CalculateDissimilarity(MotionState m1, MotionState m2)
        {
            Vector3 u = Vector3.forward;

            // b = bone length of joint
            // bRoot & b0 = 0.5
            List<float> b = new List<float>();
            foreach(float f in boneLength)
                b.Add(f);
            b[0] = 0.5f;
            
            float v1 = 0.5f * Vector3.SqrMagnitude((m1.velocity.rootDisplacement - m2.velocity.rootDisplacement));
            float v2 = b[0] * Vector3.SqrMagnitude(((m1.velocity.jointDisplacement[0] * u) - (m2.velocity.jointDisplacement[0] * u)));

            float v3 = 0.0f;
            for(int i = 1; i < m1.pose.jointRotation.Count; i++)
            {
                v3 += b[i] * Vector3.SqrMagnitude(((m1.pose.jointRotation[i] * u) - (m2.pose.jointRotation[i] * u)));
            }

            float v4 = 0.0f;
            for (int i = 1; i < m1.pose.jointRotation.Count; i++)
            {
                Quaternion q1 = m1.velocity.jointDisplacement[i] * m1.pose.jointRotation[i];
                Quaternion q2 = m2.velocity.jointDisplacement[i] * m2.pose.jointRotation[i];

                v4 += b[i] * Vector3.SqrMagnitude(((q1 * u) - (q2 * u)));
            }

            return Mathf.Sqrt(v1 + v2 + v3 + v4);
        }

        /// <summary>
        /// 3 Motion Fields
        /// 3.1 Preliminary Deﬁnitions
        /// Similarity Weights 
        /// </summary>
        private void CalculateSimilarityWeight(MotionState m)
        {
            int k = 15;
            
            float eta = 0.0f;
            for (int i = 0; i < k; i++)
            {
                KeyValuePair<int, float> n = m.neighborhood[i];

                float d = dissimilarities[m.index, n.Key];
                eta += 1 / (d * d);
            }

            Console.Out.Write("w {0} = ", m.index);
            List<float> weight = new List<float>();
            for (int i = 0; i < k; i++)
            {
                KeyValuePair<int, float> n = m.neighborhood[i];
                if (!m.similarityWeight.ContainsKey(n.Key))
                {
                    float d = dissimilarities[m.index, n.Key];
                    float w = 1 / (eta * d * d);
                    m.similarityWeight.Add(n.Key, w);
                    Console.Out.Write("{0}={1} ", n.Key, d);
                }
            }
            Console.Out.WriteLine("");
        }

        private T[,] ResizeArray<T>(T[,] original, int x, int y)
        {
            T[,] newArray = new T[x, y];
            int minX = Math.Min(original.GetLength(0), newArray.GetLength(0));
            int minY = Math.Min(original.GetLength(1), newArray.GetLength(1));

            for (int i = 0; i < minY; ++i)
                Array.Copy(original, i * original.GetLength(0), newArray, i * newArray.GetLength(0), minX);

            return newArray;
        }

    }
}
