﻿/*
 * License here
 * 
 * christyowidiasmoro (c.christyowidiasmoro@uu.nl, christyowidiasmoro@gmail.com)
 */

using System;
using MotionLibNet.Util;

namespace MotionLibNet
{
    /// <summary>
    /// Matrices are column major. Data is accessed as: row + (column*4). 
    /// Matrices can be indexed like 2D arrays but in an expression like mat[a, b], a refers to the row index, while b refers to the column index 
    /// (note that this is the opposite way round to Cartesian coordinates).
    /// </summary>
    [Serializable]
    public struct Matrix4x4
    {
        public float m00;

        public float m10;

        public float m20;

        public float m30;

        public float m01;

        public float m11;

        public float m21;

        public float m31;

        public float m02;

        public float m12;

        public float m22;

        public float m32;

        public float m03;

        public float m13;

        public float m23;

        public float m33;

        public Matrix4x4 inverse
        {
            get
            {
                return Matrix4x4.Inverse(this);
            }
        }

        public float this[int row, int column]
        {
            get
            {
                return this[row + column * 4];
            }
            set
            {
                this[row + column * 4] = value;
            }
        }

        public float this[int index]
        {
            get
            {
                float result;
                switch (index)
                {
                    case 0:
                        result = this.m00;
                        break;
                    case 1:
                        result = this.m10;
                        break;
                    case 2:
                        result = this.m20;
                        break;
                    case 3:
                        result = this.m30;
                        break;
                    case 4:
                        result = this.m01;
                        break;
                    case 5:
                        result = this.m11;
                        break;
                    case 6:
                        result = this.m21;
                        break;
                    case 7:
                        result = this.m31;
                        break;
                    case 8:
                        result = this.m02;
                        break;
                    case 9:
                        result = this.m12;
                        break;
                    case 10:
                        result = this.m22;
                        break;
                    case 11:
                        result = this.m32;
                        break;
                    case 12:
                        result = this.m03;
                        break;
                    case 13:
                        result = this.m13;
                        break;
                    case 14:
                        result = this.m23;
                        break;
                    case 15:
                        result = this.m33;
                        break;
                    default:
                        throw new System.Exception("Invalid matrix index!");
                }
                return result;
            }
            set
            {
                switch (index)
                {
                    case 0:
                        this.m00 = value;
                        break;
                    case 1:
                        this.m10 = value;
                        break;
                    case 2:
                        this.m20 = value;
                        break;
                    case 3:
                        this.m30 = value;
                        break;
                    case 4:
                        this.m01 = value;
                        break;
                    case 5:
                        this.m11 = value;
                        break;
                    case 6:
                        this.m21 = value;
                        break;
                    case 7:
                        this.m31 = value;
                        break;
                    case 8:
                        this.m02 = value;
                        break;
                    case 9:
                        this.m12 = value;
                        break;
                    case 10:
                        this.m22 = value;
                        break;
                    case 11:
                        this.m32 = value;
                        break;
                    case 12:
                        this.m03 = value;
                        break;
                    case 13:
                        this.m13 = value;
                        break;
                    case 14:
                        this.m23 = value;
                        break;
                    case 15:
                        this.m33 = value;
                        break;
                    default:
                        throw new System.Exception("Invalid matrix index!");
                }
            }
        }

        public static Matrix4x4 zero
        {
            get
            {
                return new Matrix4x4
                {
                    m00 = 0f,
                    m01 = 0f,
                    m02 = 0f,
                    m03 = 0f,
                    m10 = 0f,
                    m11 = 0f,
                    m12 = 0f,
                    m13 = 0f,
                    m20 = 0f,
                    m21 = 0f,
                    m22 = 0f,
                    m23 = 0f,
                    m30 = 0f,
                    m31 = 0f,
                    m32 = 0f,
                    m33 = 0f
                };
            }
        }

        public static Matrix4x4 identity
        {
            get
            {
                return new Matrix4x4
                {
                    m00 = 1f,
                    m01 = 0f,
                    m02 = 0f,
                    m03 = 0f,
                    m10 = 0f,
                    m11 = 1f,
                    m12 = 0f,
                    m13 = 0f,
                    m20 = 0f,
                    m21 = 0f,
                    m22 = 1f,
                    m23 = 0f,
                    m30 = 0f,
                    m31 = 0f,
                    m32 = 0f,
                    m33 = 1f
                };
            }
        }


        public static Matrix4x4 NaN
        {
            get
            {
                return new Matrix4x4
                {
                    m00 = float.NaN,
                    m01 = float.NaN,
                    m02 = float.NaN,
                    m03 = float.NaN,
                    m10 = float.NaN,
                    m11 = float.NaN,
                    m12 = float.NaN,
                    m13 = float.NaN,
                    m20 = float.NaN,
                    m21 = float.NaN,
                    m22 = float.NaN,
                    m23 = float.NaN,
                    m30 = float.NaN,
                    m31 = float.NaN,
                    m32 = float.NaN,
                    m33 = float.NaN
                };
            }
        }


        public static Matrix4x4 operator *(Matrix4x4 lhs, Matrix4x4 rhs)
        {
            return new Matrix4x4
            {
                m00 = lhs.m00 * rhs.m00 + lhs.m01 * rhs.m10 + lhs.m02 * rhs.m20 + lhs.m03 * rhs.m30,
                m01 = lhs.m00 * rhs.m01 + lhs.m01 * rhs.m11 + lhs.m02 * rhs.m21 + lhs.m03 * rhs.m31,
                m02 = lhs.m00 * rhs.m02 + lhs.m01 * rhs.m12 + lhs.m02 * rhs.m22 + lhs.m03 * rhs.m32,
                m03 = lhs.m00 * rhs.m03 + lhs.m01 * rhs.m13 + lhs.m02 * rhs.m23 + lhs.m03 * rhs.m33,
                m10 = lhs.m10 * rhs.m00 + lhs.m11 * rhs.m10 + lhs.m12 * rhs.m20 + lhs.m13 * rhs.m30,
                m11 = lhs.m10 * rhs.m01 + lhs.m11 * rhs.m11 + lhs.m12 * rhs.m21 + lhs.m13 * rhs.m31,
                m12 = lhs.m10 * rhs.m02 + lhs.m11 * rhs.m12 + lhs.m12 * rhs.m22 + lhs.m13 * rhs.m32,
                m13 = lhs.m10 * rhs.m03 + lhs.m11 * rhs.m13 + lhs.m12 * rhs.m23 + lhs.m13 * rhs.m33,
                m20 = lhs.m20 * rhs.m00 + lhs.m21 * rhs.m10 + lhs.m22 * rhs.m20 + lhs.m23 * rhs.m30,
                m21 = lhs.m20 * rhs.m01 + lhs.m21 * rhs.m11 + lhs.m22 * rhs.m21 + lhs.m23 * rhs.m31,
                m22 = lhs.m20 * rhs.m02 + lhs.m21 * rhs.m12 + lhs.m22 * rhs.m22 + lhs.m23 * rhs.m32,
                m23 = lhs.m20 * rhs.m03 + lhs.m21 * rhs.m13 + lhs.m22 * rhs.m23 + lhs.m23 * rhs.m33,
                m30 = lhs.m30 * rhs.m00 + lhs.m31 * rhs.m10 + lhs.m32 * rhs.m20 + lhs.m33 * rhs.m30,
                m31 = lhs.m30 * rhs.m01 + lhs.m31 * rhs.m11 + lhs.m32 * rhs.m21 + lhs.m33 * rhs.m31,
                m32 = lhs.m30 * rhs.m02 + lhs.m31 * rhs.m12 + lhs.m32 * rhs.m22 + lhs.m33 * rhs.m32,
                m33 = lhs.m30 * rhs.m03 + lhs.m31 * rhs.m13 + lhs.m32 * rhs.m23 + lhs.m33 * rhs.m33
            };
        }

        public static Vector4 operator *(Matrix4x4 lhs, Vector4 v)
        {
            Vector4 result;
            result.x = lhs.m00 * v.x + lhs.m01 * v.y + lhs.m02 * v.z + lhs.m03 * v.w;
            result.y = lhs.m10 * v.x + lhs.m11 * v.y + lhs.m12 * v.z + lhs.m13 * v.w;
            result.z = lhs.m20 * v.x + lhs.m21 * v.y + lhs.m22 * v.z + lhs.m23 * v.w;
            result.w = lhs.m30 * v.x + lhs.m31 * v.y + lhs.m32 * v.z + lhs.m33 * v.w;
            return result;
        }

        public static bool operator ==(Matrix4x4 lhs, Matrix4x4 rhs)
        {
            return lhs.GetColumn(0) == rhs.GetColumn(0) && lhs.GetColumn(1) == rhs.GetColumn(1) && lhs.GetColumn(2) == rhs.GetColumn(2) && lhs.GetColumn(3) == rhs.GetColumn(3);
        }

        public static bool operator !=(Matrix4x4 lhs, Matrix4x4 rhs)
        {
            return !(lhs == rhs);
        }

        public Vector4 GetColumn(int i)
        {
            return new Vector4(this[0, i], this[1, i], this[2, i], this[3, i]);
        }

        public Vector4 GetRow(int i)
        {
            return new Vector4(this[i, 0], this[i, 1], this[i, 2], this[i, 3]);
        }

        public void SetColumn(int i, Vector4 v)
        {
            this[0, i] = v.x;
            this[1, i] = v.y;
            this[2, i] = v.z;
            this[3, i] = v.w;
        }

        public void SetRow(int i, Vector4 v)
        {
            this[i, 0] = v.x;
            this[i, 1] = v.y;
            this[i, 2] = v.z;
            this[i, 3] = v.w;
        }

        public static Matrix4x4 Inverse(Matrix4x4 mat)
        {
            //                                       -1
            // If you have matrix M, inverse Matrix M   can compute
            //
            //     -1       1      
            //    M   = --------- A
            //            det(M)
            //
            // A is adjugate (adjoint) of M, where,
            //
            //      T
            // A = C
            //
            // C is Cofactor matrix of M, where,
            //           i + j
            // C   = (-1)      * det(M  )
            //  ij                    ij
            //
            //     [ a b c d ]
            // M = [ e f g h ]
            //     [ i j k l ]
            //     [ m n o p ]
            //
            // First Row
            //           2 | f g h |
            // C   = (-1)  | j k l | = + ( f ( kp - lo ) - g ( jp - ln ) + h ( jo - kn ) )
            //  11         | n o p |
            //
            //           3 | e g h |
            // C   = (-1)  | i k l | = - ( e ( kp - lo ) - g ( ip - lm ) + h ( io - km ) )
            //  12         | m o p |
            //
            //           4 | e f h |
            // C   = (-1)  | i j l | = + ( e ( jp - ln ) - f ( ip - lm ) + h ( in - jm ) )
            //  13         | m n p |
            //
            //           5 | e f g |
            // C   = (-1)  | i j k | = - ( e ( jo - kn ) - f ( io - km ) + g ( in - jm ) )
            //  14         | m n o |
            //
            // Second Row
            //           3 | b c d |
            // C   = (-1)  | j k l | = - ( b ( kp - lo ) - c ( jp - ln ) + d ( jo - kn ) )
            //  21         | n o p |
            //
            //           4 | a c d |
            // C   = (-1)  | i k l | = + ( a ( kp - lo ) - c ( ip - lm ) + d ( io - km ) )
            //  22         | m o p |
            //
            //           5 | a b d |
            // C   = (-1)  | i j l | = - ( a ( jp - ln ) - b ( ip - lm ) + d ( in - jm ) )
            //  23         | m n p |
            //
            //           6 | a b c |
            // C   = (-1)  | i j k | = + ( a ( jo - kn ) - b ( io - km ) + c ( in - jm ) )
            //  24         | m n o |
            //
            // Third Row
            //           4 | b c d |
            // C   = (-1)  | f g h | = + ( b ( gp - ho ) - c ( fp - hn ) + d ( fo - gn ) )
            //  31         | n o p |
            //
            //           5 | a c d |
            // C   = (-1)  | e g h | = - ( a ( gp - ho ) - c ( ep - hm ) + d ( eo - gm ) )
            //  32         | m o p |
            //
            //           6 | a b d |
            // C   = (-1)  | e f h | = + ( a ( fp - hn ) - b ( ep - hm ) + d ( en - fm ) )
            //  33         | m n p |
            //
            //           7 | a b c |
            // C   = (-1)  | e f g | = - ( a ( fo - gn ) - b ( eo - gm ) + c ( en - fm ) )
            //  34         | m n o |
            //
            // Fourth Row
            //           5 | b c d |
            // C   = (-1)  | f g h | = - ( b ( gl - hk ) - c ( fl - hj ) + d ( fk - gj ) )
            //  41         | j k l |
            //
            //           6 | a c d |
            // C   = (-1)  | e g h | = + ( a ( gl - hk ) - c ( el - hi ) + d ( ek - gi ) )
            //  42         | i k l |
            //
            //           7 | a b d |
            // C   = (-1)  | e f h | = - ( a ( fl - hj ) - b ( el - hi ) + d ( ej - fi ) )
            //  43         | i j l |
            //
            //           8 | a b c |
            // C   = (-1)  | e f g | = + ( a ( fk - gj ) - b ( ek - gi ) + c ( ej - fi ) )
            //  44         | i j k |
            //
            // Cost of operation
            // 53 adds, 104 muls, and 1 div.

            Matrix4x4 result;

            float a = mat.m00, b = mat.m01, c = mat.m02, d = mat.m03;
            float e = mat.m10, f = mat.m11, g = mat.m12, h = mat.m13;
            float i = mat.m20, j = mat.m21, k = mat.m22, l = mat.m23;
            float m = mat.m30, n = mat.m31, o = mat.m32, p = mat.m33;

            float kp_lo = k * p - l * o;
            float jp_ln = j * p - l * n;
            float jo_kn = j * o - k * n;
            float ip_lm = i * p - l * m;
            float io_km = i * o - k * m;
            float in_jm = i * n - j * m;

            float a11 = +(f * kp_lo - g * jp_ln + h * jo_kn);
            float a12 = -(e * kp_lo - g * ip_lm + h * io_km);
            float a13 = +(e * jp_ln - f * ip_lm + h * in_jm);
            float a14 = -(e * jo_kn - f * io_km + g * in_jm);

            float det = a * a11 + b * a12 + c * a13 + d * a14;

            if (Mathf.Abs(det) < float.Epsilon)
            {
                return Matrix4x4.NaN;
            }

            float invDet = 1.0f / det;

            result.m00 = a11 * invDet;
            result.m10 = a12 * invDet;
            result.m20 = a13 * invDet;
            result.m30 = a14 * invDet;

            result.m01 = -(b * kp_lo - c * jp_ln + d * jo_kn) * invDet;
            result.m11 = +(a * kp_lo - c * ip_lm + d * io_km) * invDet;
            result.m21 = -(a * jp_ln - b * ip_lm + d * in_jm) * invDet;
            result.m31 = +(a * jo_kn - b * io_km + c * in_jm) * invDet;

            float gp_ho = g * p - h * o;
            float fp_hn = f * p - h * n;
            float fo_gn = f * o - g * n;
            float ep_hm = e * p - h * m;
            float eo_gm = e * o - g * m;
            float en_fm = e * n - f * m;

            result.m02 = +(b * gp_ho - c * fp_hn + d * fo_gn) * invDet;
            result.m12 = -(a * gp_ho - c * ep_hm + d * eo_gm) * invDet;
            result.m22 = +(a * fp_hn - b * ep_hm + d * en_fm) * invDet;
            result.m32 = -(a * fo_gn - b * eo_gm + c * en_fm) * invDet;

            float gl_hk = g * l - h * k;
            float fl_hj = f * l - h * j;
            float fk_gj = f * k - g * j;
            float el_hi = e * l - h * i;
            float ek_gi = e * k - g * i;
            float ej_fi = e * j - f * i;

            result.m03 = -(b * gl_hk - c * fl_hj + d * fk_gj) * invDet;
            result.m13 = +(a * gl_hk - c * el_hi + d * ek_gi) * invDet;
            result.m23 = -(a * fl_hj - b * el_hi + d * ej_fi) * invDet;
            result.m33 = +(a * fk_gj - b * ek_gi + c * ej_fi) * invDet;

            return result;
        }

        /*
        public static Matrix4x4 CreateTranslation(Vector3 position)
        {
            Matrix4x4 result = Matrix4x4.identity;

            result.m03 = position.x;
            result.m13 = position.y;
            result.m23 = position.z;
            result.m33 = 1.0f;

            return result;
        }

        public static Matrix4x4 CreateScale(Vector3 scales)
        {
            Matrix4x4 result;

            result.m00 = scales.x;
            result.m10 = 0.0f;
            result.m20 = 0.0f;
            result.m30 = 0.0f;
            result.m01 = 0.0f;
            result.m11 = scales.y;
            result.m21 = 0.0f;
            result.m31 = 0.0f;
            result.m02 = 0.0f;
            result.m12 = 0.0f;
            result.m22 = scales.z;
            result.m32 = 0.0f;
            result.m03 = 0.0f;
            result.m13 = 0.0f;
            result.m23 = 0.0f;
            result.m33 = 1.0f;

            return result;
        }

        public static Matrix4x4 CreateRotation(Quaternion q)
        {
            Vector3 axis;
            float angle;
            q.ToAxisAngle(out axis, out angle);
            return CreateRotation(axis, angle);
        }

        public static Matrix4x4 CreateRotation(Vector3 axis, float angle)
        {
            // http://www.euclideanspace.com/maths/geometry/rotations/conversions/angleToMatrix/index.htm

            float cos = Mathf.Cos(-angle);
            float sin = Mathf.Sin(-angle);
            float t = 1.0f - cos;

            axis.Normalize();

            Matrix4x4 result = Matrix4x4.zero;
            result.SetRow(0, new Vector4(t * axis.x * axis.x + cos, t * axis.x * axis.y - sin * axis.z, t * axis.x * axis.z + sin * axis.y, 0.0f));
            result.SetRow(1, new Vector4(t * axis.x * axis.y + sin * axis.z, t * axis.y * axis.y + cos, t * axis.y * axis.z - sin * axis.x, 0.0f));
            result.SetRow(2, new Vector4(t * axis.x * axis.z - sin * axis.y, t * axis.y * axis.z + sin * axis.x, t * axis.z * axis.z + cos, 0.0f));
            result.SetRow(3, new Vector4(0, 0, 0, 1));
            return result;
        }
        */

        public static Matrix4x4 TRS(Vector3 pos, Quaternion q, Vector3 s)
        {
            // http://www.euclideanspace.com/maths/geometry/rotations/conversions/angleToMatrix/index.htm

            Vector3 axis;
            float angle;
            q.ToAxisAngle(out axis, out angle);

            float cos = Mathf.Cos(angle * Mathf.Deg2Rad);
            float sin = Mathf.Sin(angle * Mathf.Deg2Rad);
            float t = 1.0f - cos;

            Matrix4x4 result = Matrix4x4.identity;

            result.m00 = (t * axis.x * axis.x + cos) * s.x;
            result.m11 = (t * axis.y * axis.y + cos) * s.y;
            result.m22 = (t * axis.z * axis.z + cos) * s.z;

            float tmp1 = axis.x * axis.y * t;
            float tmp2 = axis.z * sin;
            result.m10 = (tmp1 + tmp2) * s.x;
            result.m01 = (tmp1 - tmp2) * s.y;
            tmp1 = axis.x * axis.z * t;
            tmp2 = axis.y * sin;
            result.m20 = (tmp1 - tmp2) * s.x;
            result.m02 = (tmp1 + tmp2) * s.z;
            tmp1 = axis.y * axis.z * t;
            tmp2 = axis.x * sin;
            result.m21 = (tmp1 + tmp2) * s.y;
            result.m12 = (tmp1 - tmp2) * s.z;

            result.m03 = pos.x;
            result.m13 = pos.y;
            result.m23 = pos.z;
            result.m33 = 1.0f;

            return result;
        }

        public override int GetHashCode()
        {
            return this.GetColumn(0).GetHashCode() ^ this.GetColumn(1).GetHashCode() << 2 ^ this.GetColumn(2).GetHashCode() >> 2 ^ this.GetColumn(3).GetHashCode() >> 1;
        }

        public override bool Equals(object other)
        {
            bool result;
            if (!(other is Matrix4x4))
            {
                result = false;
            }
            else
            {
                Matrix4x4 matrix4x = (Matrix4x4)other;
                result = (this.GetColumn(0).Equals(matrix4x.GetColumn(0)) && this.GetColumn(1).Equals(matrix4x.GetColumn(1)) && this.GetColumn(2).Equals(matrix4x.GetColumn(2)) && this.GetColumn(3).Equals(matrix4x.GetColumn(3)));
            }
            return result;
        }

        public override string ToString()
        {
            return string.Format("{0:F5}\t{1:F5}\t{2:F5}\t{3:F5}\n{4:F5}\t{5:F5}\t{6:F5}\t{7:F5}\n{8:F5}\t{9:F5}\t{10:F5}\t{11:F5}\n{12:F5}\t{13:F5}\t{14:F5}\t{15:F5}\n", new object[]
            {
                this.m00,
                this.m01,
                this.m02,
                this.m03,
                this.m10,
                this.m11,
                this.m12,
                this.m13,
                this.m20,
                this.m21,
                this.m22,
                this.m23,
                this.m30,
                this.m31,
                this.m32,
                this.m33
            });
        }
    }
}
