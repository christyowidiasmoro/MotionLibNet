﻿/*
 * License here
 * 
 * christyowidiasmoro (c.christyowidiasmoro@uu.nl, christyowidiasmoro@gmail.com)
 */

using System;
using System.Collections.Generic;

namespace MotionLibNet
{
    /// <summary>
    /// Motion Data stuctures
    /// heavily inspired from BVH motion data structures
    /// </summary>
    [Serializable]
    public class Motion
    {
        /// <summary>
        /// 
        /// </summary>
        public string Name;

        /// <summary>
        /// 
        /// </summary>
        public int ChannelStart = 0;

        /// <summary>
        /// 
        /// </summary>
        public Joint RootJoint = new Joint();

        /// <summary>
        /// 
        /// </summary>
        public int NumFrames;

        /// <summary>
        /// 
        /// </summary>
        public int NumMotionChannels = 0;

        /// <summary>
        /// 
        /// </summary>
        public float FrameTime;

        /// <summary>
        /// 
        /// </summary>
        public float[] Data;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="motion"></param>
        /// <param name="joint"></param>
        public delegate void OnJointUpdated(Motion motion, Joint joint);

        /// <summary>
        /// 
        /// </summary>
        public OnJointUpdated onJointUpdated = null;

        /// <summary>
        /// copy this motion
        /// </summary>
        /// <returns></returns>
        public Motion Clone()
        {
            Motion motion = CopyStructure();
            if (Data != null)
            {
                motion.Data = new float[Data.Length];
                for (int i = 0; i < Data.Length; i++)
                    motion.Data[i] = Data[i];
            }
            return motion;
        }

        /// <summary>
        /// copy the sturecture of this motion (without data)
        /// </summary>
        /// <returns></returns>
        public Motion CopyStructure()
        {
            Motion motion = new Motion();
            motion.Name = Name;
            motion.ChannelStart = ChannelStart;
            motion.RootJoint = RootJoint.Clone();
            motion.NumFrames = NumFrames;
            motion.NumMotionChannels = NumMotionChannels;
            motion.FrameTime = FrameTime;

            return motion;
        }

        /// <summary>
        /// update joint, need to be called after calling MoveTo function to update all related joints
        /// </summary>
        /// <param name="joint"></param>
        public void UpdateJointObject(Joint joint = null)
        {
            if (joint == null)
                joint = RootJoint;

            if (onJointUpdated != null)
            {
                onJointUpdated(this, joint);
            }

            foreach (Joint child in joint.Children)
                UpdateJointObject(child);
        }

        /// <summary>
        /// insert motion float data to specific frame
        /// </summary>
        /// <param name="frame"></param>
        /// <param name="data"></param>
        public void InsertTo(int frame, float[] data)
        {
            int startIndex = frame * NumMotionChannels;
            int oldLength = 0;
            if (this.Data == null)
            {
                this.Data = new float[startIndex + data.Length];
            }
            else
            {
                oldLength = this.Data.Length;
                Array.Resize(ref this.Data, this.Data.Length + data.Length);
            }
            NumFrames = this.Data.Length / NumMotionChannels - 1;

            Array.Copy(this.Data, startIndex, this.Data, startIndex + data.Length, oldLength - startIndex);

            Array.Copy(data, 0, this.Data, startIndex, data.Length);
        }

        /// <summary>
        /// insert joint data to specific frame
        /// </summary>
        /// <param name="frame"></param>
        /// <param name="joint"></param>
        public void InsertTo(int frame, Joint joint)
        {
            List<float> values = new List<float>();

            RecordJoint(joint, ref values);

            int startIndex = frame * NumMotionChannels;
            int oldLength = 0;

            if (Data == null)
            {
                Data = new float[startIndex + values.Count];
            }
            else
            {
                oldLength = this.Data.Length;
                Array.Resize(ref this.Data, this.Data.Length + values.Count);
            }
            NumFrames = this.Data.Length / NumMotionChannels - 1;

            Array.Copy(this.Data, startIndex, this.Data, startIndex + values.Count, oldLength - startIndex);

            for (int i = 0; i < values.Count; i++)
            {
                Data[startIndex + i] = values[i];
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="frame"></param>
        /// <param name="data"></param>
        public void RecordTo(int frame, float[] data)
        {
            int startIndex = frame * NumMotionChannels;
            if (this.Data == null)
            {
                this.Data = new float[startIndex + data.Length];
            }
            else if (this.Data.Length < startIndex + data.Length)
            {
                Array.Resize(ref this.Data, startIndex + data.Length);
            }
            NumFrames = this.Data.Length / NumMotionChannels - 1;

            Array.Copy(data, 0, this.Data, startIndex, data.Length);
        }

        /// <summary>
        /// get joint data from this motion
        /// </summary>
        /// <param name="frame"></param>
        /// <param name="joint"></param>
        public void RecordTo(int frame, Joint joint)
        {
            List<float> values = new List<float>();

            RecordJoint(joint, ref values);

            int startIndex = frame * NumMotionChannels;

            if (Data == null)
            {
                Data = new float[startIndex + values.Count];
                NumFrames = 1;
            }
            else
            {
                if (Data.Length < startIndex + values.Count)
                    Array.Resize(ref Data, startIndex + values.Count);
            }
            NumFrames = this.Data.Length / NumMotionChannels - 1;

            for (int i = 0; i < values.Count; i++)
            {
                Data[startIndex + i] = values[i];

            }
        }

        /// <summary>
        /// get motion values data from this motion
        /// </summary>
        /// <param name="joint"></param>
        /// <param name="values"></param>
        private void RecordJoint(Joint joint, ref List<float> values)
        {
            joint.Matrix = Matrix4x4.TRS(joint.Position, joint.Rotation, Vector3.one);

            if (joint.Parent != null)
            {
                joint.Parent.Matrix = Matrix4x4.TRS(joint.Parent.Position, joint.Parent.Rotation, Vector3.one);

                joint.Matrix = joint.Parent.Matrix.inverse * joint.Matrix;
            }

            Vector3 position = joint.Matrix.GetColumn(3);
            Quaternion rotation = Quaternion.FromMatrix(joint.Matrix);

            Vector3 axis;
            float angle;
            rotation.ToAxisAngle(out axis, out angle);
            Vector3 angleRotation = axis * angle;

            for (int i = 0; i < joint.NumberofChannels; i++)
            {
                // channel alias
                Joint.Channel channel = joint.ChannelsOrder[i];
                float value = 0.0f;

                if (channel == Joint.Channel.Xposition)
                {
                    value = position.x;
                }
                if (channel == Joint.Channel.Yposition)
                {
                    value = position.y;
                }
                if (channel == Joint.Channel.Zposition)
                {
                    value = position.z;
                }

                if (channel == Joint.Channel.Xrotation)
                {
                    value = angleRotation.x;
                }
                if (channel == Joint.Channel.Yrotation)
                {
                    value = angleRotation.y;
                }
                if (channel == Joint.Channel.Zrotation)
                {
                    value = angleRotation.z;
                }

                values.Add(value);
            }

            foreach (Joint child in joint.Children)
                RecordJoint(child, ref values);
        }

        /// <summary>
        /// move joint to specific frame
        /// </summary>
        /// <param name="frame"></param>
        /// <param name="joint"></param>
        public void MoveTo(int frame, out Joint joint)
        {
            if (frame >= NumFrames)
                frame = NumFrames - 1;
            if (frame < 0)
                frame = 0;

            // we calculate motion data's array start index for a frame
            int startIndex = frame * NumMotionChannels;

            // recursively transform skeleton
            MoveJoint(ref RootJoint, startIndex);

            joint = RootJoint;
        }

        /// <summary>
        /// move root joint to specific frame
        /// </summary>
        /// <param name="frame"></param>
        public void MoveTo(int frame)
        {
            MoveTo(frame, out RootJoint);
        }

        /// <summary>
        /// recursive move joint function
        /// </summary>
        /// <param name="joint"></param>
        /// <param name="frameStartsIndex"></param>
        private void MoveJoint(ref Joint joint, int frameStartsIndex)
        {
            // we'll need index of motion data's array with start of this specific joint
            int startIndex = frameStartsIndex + joint.ChannelStart;

            // apply offset 
            joint.Matrix = Matrix4x4.identity;

            Vector3 translation = joint.Offset;
            Quaternion rotation = Quaternion.identity;

            // here we transform joint's local matrix with each specified channel's values
            // which are read from motion data

            for (int i = 0; i < joint.NumberofChannels; i++)
            {
                // channel alias
                Joint.Channel channel = joint.ChannelsOrder[i];

                // extract value from motion data
                float value = Data[startIndex + i];

                if (channel == Joint.Channel.Xposition)
                {
                    translation += new Vector3(value, 0, 0);
                }
                if (channel == Joint.Channel.Yposition)
                {
                    translation += new Vector3(0, value, 0);
                }
                if (channel == Joint.Channel.Zposition)
                {
                    translation += new Vector3(0, 0, value);
                }

                if (channel == Joint.Channel.Xrotation)
                {
                    rotation *= Quaternion.AngleAxis(value, Vector3.right);
                }
                if (channel == Joint.Channel.Yrotation)
                {
                    rotation *= Quaternion.AngleAxis(value, Vector3.up);
                }
                if (channel == Joint.Channel.Zrotation)
                {
                    rotation *= Quaternion.AngleAxis(value, Vector3.forward);
                }
            }

            // apply translation and / or rotation to current matrix
            joint.Matrix = Matrix4x4.TRS(translation, rotation, Vector3.one);

            // then we apply parent's local transfomation matrix to this joint's LTM (local tr. mtx. :)
            if (joint.Parent != null)
            {
                // apply parent matrix to current matrix
                joint.Matrix = joint.Parent.Matrix * joint.Matrix;
            }

            joint.Position = joint.Matrix.GetColumn(3);
            joint.Rotation = Quaternion.FromMatrix(joint.Matrix);

            // when we have calculated parent's matrix do the same to all children
            for (int i = 0; i < joint.Children.Count; i++)
            {
                Joint j = (Joint)joint.Children[i];

                MoveJoint(ref j, frameStartsIndex);

                joint.Children[i] = j;
            }
        }
    }
}
