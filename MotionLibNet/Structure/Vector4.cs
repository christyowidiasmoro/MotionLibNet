﻿/*
 * License here
 * 
 * christyowidiasmoro (c.christyowidiasmoro@uu.nl, christyowidiasmoro@gmail.com)
 */

using System;
using MotionLibNet.Util;

namespace MotionLibNet
{
    /// <summary>
    /// from Unity3D Vector4 specification
    /// </summary>
    [Serializable]
    public struct Vector4
    {
        public const float kEpsilon = 1E-05f;

        public float x;

        public float y;

        public float z;

        public float w;

        public float this[int index]
        {
            get
            {
                float result;
                switch (index)
                {
                    case 0:
                        result = this.x;
                        break;
                    case 1:
                        result = this.y;
                        break;
                    case 2:
                        result = this.z;
                        break;
                    case 3:
                        result = this.w;
                        break;
                    default:
                        throw new System.Exception("Invalid Vector4 index!");
                }
                return result;
            }
            set
            {
                switch (index)
                {
                    case 0:
                        this.x = value;
                        break;
                    case 1:
                        this.y = value;
                        break;
                    case 2:
                        this.z = value;
                        break;
                    case 3:
                        this.w = value;
                        break;
                    default:
                        throw new System.Exception("Invalid Vector4 index!");
                }
            }
        }

        public Vector4 normalized
        {
            get
            {
                return Vector4.Normalize(this);
            }
        }

        public float magnitude
        {
            get
            {
                return (float)Mathf.Sqrt(Vector4.Dot(this, this));
            }
        }

        public float sqrMagnitude
        {
            get
            {
                return Vector4.Dot(this, this);
            }
        }

        public static Vector4 zero
        {
            get
            {
                return new Vector4(0f, 0f, 0f, 0f);
            }
        }

        public static Vector4 one
        {
            get
            {
                return new Vector4(1f, 1f, 1f, 1f);
            }
        }

        public Vector4(float x, float y, float z, float w)
        {
            this.x = x;
            this.y = y;
            this.z = z;
            this.w = w;
        }

        public Vector4(float x, float y, float z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
            this.w = 0f;
        }

        public Vector4(float x, float y)
        {
            this.x = x;
            this.y = y;
            this.z = 0f;
            this.w = 0f;
        }

        public static Vector4 Normalize(Vector4 a)
        {
            float num = Vector4.Magnitude(a);
            Vector4 result;
            if (num > 1E-05f)
            {
                result = a / num;
            }
            else
            {
                result = Vector4.zero;
            }
            return result;
        }

        public void Normalize()
        {
            float num = Vector4.Magnitude(this);
            if (num > 1E-05f)
            {
                this /= num;
            }
            else
            {
                this = Vector4.zero;
            }
        }

        public static float Dot(Vector4 a, Vector4 b)
        {
            return a.x * b.x + a.y * b.y + a.z * b.z + a.w * b.w;
        }
        public static Vector4 Project(Vector4 a, Vector4 b)
        {
            return b * Vector4.Dot(a, b) / Vector4.Dot(b, b);
        }

        public static float Distance(Vector4 a, Vector4 b)
        {
            return Vector4.Magnitude(a - b);
        }

        public static float Magnitude(Vector4 a)
        {
            return (float)Mathf.Sqrt(Vector4.Dot(a, a));
        }

        public static Vector4 Min(Vector4 lhs, Vector4 rhs)
        {
            return new Vector4((float)Mathf.Min(lhs.x, rhs.x), (float)Mathf.Min(lhs.y, rhs.y), (float)Mathf.Min(lhs.z, rhs.z), (float)Mathf.Min(lhs.w, rhs.w));
        }

        public static Vector4 Max(Vector4 lhs, Vector4 rhs)
        {
            return new Vector4((float)Mathf.Max(lhs.x, rhs.x), (float)Mathf.Max(lhs.y, rhs.y), (float)Mathf.Max(lhs.z, rhs.z), (float)Mathf.Max(lhs.w, rhs.w));
        }

        public static Vector4 operator +(Vector4 a, Vector4 b)
        {
            return new Vector4(a.x + b.x, a.y + b.y, a.z + b.z, a.w + b.w);
        }

        public static Vector4 operator -(Vector4 a, Vector4 b)
        {
            return new Vector4(a.x - b.x, a.y - b.y, a.z - b.z, a.w - b.w);
        }

        public static Vector4 operator -(Vector4 a)
        {
            return new Vector4(-a.x, -a.y, -a.z, -a.w);
        }

        public static Vector4 operator *(Vector4 a, float d)
        {
            return new Vector4(a.x * d, a.y * d, a.z * d, a.w * d);
        }

        public static Vector4 operator *(float d, Vector4 a)
        {
            return new Vector4(a.x * d, a.y * d, a.z * d, a.w * d);
        }

        public static Vector4 operator /(Vector4 a, float d)
        {
            return new Vector4(a.x / d, a.y / d, a.z / d, a.w / d);
        }

        public static bool operator ==(Vector4 lhs, Vector4 rhs)
        {
            return Vector4.SqrMagnitude(lhs - rhs) < 9.99999944E-11f;
        }

        public static bool operator !=(Vector4 lhs, Vector4 rhs)
        {
            return Vector4.SqrMagnitude(lhs - rhs) >= 9.99999944E-11f;
        }

        public static implicit operator Vector4(Vector3 v)
        {
            return new Vector4(v.x, v.y, v.z, 0f);
        }

        public static implicit operator Vector3(Vector4 v)
        {
            return new Vector3(v.x, v.y, v.z);
        }

        public static implicit operator Vector4(Vector2 v)
        {
            return new Vector4(v.x, v.y, 0f, 0f);
        }

        public static implicit operator Vector2(Vector4 v)
        {
            return new Vector2(v.x, v.y);
        }
        public override int GetHashCode()
        {
            return this.x.GetHashCode() ^ this.y.GetHashCode() << 2 ^ this.z.GetHashCode() >> 2 ^ this.w.GetHashCode() >> 1;
        }

        public override bool Equals(object other)
        {
            bool result;
            if (!(other is Vector4))
            {
                result = false;
            }
            else
            {
                Vector4 vector = (Vector4)other;
                result = (this.x.Equals(vector.x) && this.y.Equals(vector.y) && this.z.Equals(vector.z) && this.w.Equals(vector.w));
            }
            return result;
        }

        public override string ToString()
        {
            return string.Format("({0:F1}, {1:F1}, {2:F1}, {3:F1})", new object[]
            {
                this.x,
                this.y,
                this.z,
                this.w
            });
        }

        public string ToString(string format)
        {
            return string.Format("({0}, {1}, {2}, {3})", new object[]
            {
                this.x.ToString(format),
                this.y.ToString(format),
                this.z.ToString(format),
                this.w.ToString(format)
            });
        }

        public static float SqrMagnitude(Vector4 a)
        {
            return Vector4.Dot(a, a);
        }

        public float SqrMagnitude()
        {
            return Vector4.Dot(this, this);
        }
    }
}