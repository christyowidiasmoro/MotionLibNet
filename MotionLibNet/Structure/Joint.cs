﻿/*
 * License here
 * 
 * christyowidiasmoro (c.christyowidiasmoro@uu.nl, christyowidiasmoro@gmail.com)
 */

using System;
using System.Collections;
using System.Collections.Generic;

namespace MotionLibNet
{
    /// <summary>
    /// joint data structures
    /// inspired from BVH data structures
    /// </summary>
    [Serializable]
    public class Joint
    {
        /// <summary>
        /// 
        /// </summary>
        public enum Channel
        {
            Xposition = 1,
            Yposition = 2,
            Zposition = 4,
            Zrotation = 10,
            Xrotation = 20,
            Yrotation = 40
        };

        /// <summary>
        /// BVH channel string
        /// </summary>
        /// <param name="c"></param>
        /// <returns></returns>
        public static string ChannelString(Channel c)
        {
            switch(c)
            {
                case Channel.Xposition:
                    return "Xposition";
                case Channel.Yposition:
                    return "Yposition";
                case Channel.Zposition:
                    return "Zposition";
                case Channel.Zrotation:
                    return "Zrotation";
                case Channel.Xrotation:
                    return "Xrotation";
                case Channel.Yrotation:
                    return "Yrotation";
                default:
                    return "";
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public string Name;

        /// <summary>
        /// 
        /// </summary>
        public Joint Parent = null;

        /// <summary>
        /// 
        /// </summary>
        public Vector3 Offset;

        /// <summary>
        /// 
        /// </summary>
        public int NumberofChannels;

        /// <summary>
        /// 
        /// </summary>
        public ArrayList Children = new ArrayList();

        /// <summary>
        /// 
        /// </summary>
        public Matrix4x4 Matrix;

        /// <summary>
        /// 
        /// </summary>
        public Vector3 Position;

        /// <summary>
        /// 
        /// </summary>
        public Quaternion Rotation;

        /// <summary>
        /// 
        /// </summary>
        public int ChannelStart;

        /// <summary>
        /// 
        /// </summary>
        public Channel[] ChannelsOrder;
        
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public Joint Clone()
        {
            Joint joint = new Joint();

            joint.Name = Name;
            joint.Parent = Parent;  // keep by references
            joint.Offset = Offset;
            joint.NumberofChannels = NumberofChannels;
            joint.Matrix = Matrix;
            joint.Position = Position;
            joint.Rotation = Rotation;
            joint.ChannelStart = ChannelStart;
            if (ChannelsOrder == null)
                joint.ChannelsOrder = null;
            else
            {
                joint.ChannelsOrder = new Channel[ChannelsOrder.Length];
                for (int i = 0; i < ChannelsOrder.Length; i++)
                {
                    joint.ChannelsOrder[i] = ChannelsOrder[i];
                }
            }
            joint.Children = new ArrayList();
            foreach(Joint child in Children)
            {
                Joint j = child.Clone();
                j.Parent = joint;   // override the reference to a new object
                joint.Children.Add(j);
            }

            return joint;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="positions"></param>
        public void ExtractJointPosition(ref List<Vector3> positions)
        {
            positions.Add(Position);

            foreach (Joint child in Children)
                child.ExtractJointPosition(ref positions);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rotations"></param>
        public void ExtractJointRotation(ref List<Quaternion> rotations)
        {
            rotations.Add(Rotation);

            foreach (Joint child in Children)
                child.ExtractJointRotation(ref rotations);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="positions"></param>
        /// <param name="rotations"></param>
        /// <param name="offsets"></param>
        public void ExtractJoint(ref List<Vector3> positions, ref List<Quaternion> rotations, ref List<Vector3> offsets)
        {
            positions.Add(Position);

            rotations.Add(Rotation);

            offsets.Add(Offset);

            foreach (Joint child in Children)
                child.ExtractJoint(ref positions, ref rotations, ref offsets);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="lengths"></param>
        public void ExtractBoneLength(ref List<float> lengths)
        {
            float l = 0;
            if (Parent != null)
                l = (Parent.Position - Position).magnitude;

            lengths.Add(l);

            foreach (Joint child in Children)
                child.ExtractBoneLength(ref lengths);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="index"></param>
        /// <param name="positions"></param>
        /// <param name="rotations"></param>
        public void SetValue(ref int index, List<Vector3> positions, List<Quaternion> rotations)
        {
            Position = positions[index];
            Rotation = rotations[index];
            index++;

            foreach (Joint child in Children)
                child.SetValue(ref index, positions, rotations);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="jointIndex"></param>
        /// <param name="rotation"></param>
        public void SetRotation(ref int jointIndex, Quaternion rotation)
        {
            if (jointIndex == 0)
                Rotation = rotation;
            else
            {
                foreach (Joint child in Children)
                {
                    jointIndex--;
                    child.SetRotation(ref jointIndex, rotation);
                }
            }
        }

    }
}
