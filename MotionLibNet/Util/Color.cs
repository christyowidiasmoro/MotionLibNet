﻿/*
 * License here
 * 
 * christyowidiasmoro (c.christyowidiasmoro@uu.nl, christyowidiasmoro@gmail.com)
 */
 
namespace MotionLibNet
{
    /// <summary>
    /// coloring function, emulate color index from GraphDot specification
    /// </summary>
    public struct Color
    {
        /// <summary>
        /// red value
        /// </summary>
        public float r;

        /// <summary>
        /// green value
        /// </summary>
        public float g;

        /// <summary>
        /// blue value
        /// </summary>
        public float b;

        /// <summary>
        /// GraphDot Set312
        /// </summary>
        public static string[] Set312 = new string[]
        {
            "#8dd3c7", "#ffffb3", "#bebada", "#fb8072", "#80b1d3", "#fdb462", "#b3de69", "#fccde5", "#d9d9d9", "#bc80bd", "#ccebc5", "#ffed6f"
        };

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("#{0:X2}{1:X2}{2:X2}",
                (int)(r * 255),
                (int)(g * 255),
                (int)(b * 255));
        }
    }
}
